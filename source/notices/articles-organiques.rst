:orphan:
.. index::
   single: Articles organiques
.. _Articles organiques:


====================
articles organiques
====================


→ :ref:`Alsace-Moselle` ; :ref:`concordat de 1801` ; :ref:`fabrique` ; :ref:`police des cultes` ; :ref:`propriété` 

Présentation générale
=====================

Restaurant la paix religieuse en France, Bonaparte ajouta au :ref:`Concordat <concordat>` signé avec le pape Pie VII, des Articles organiques relatifs aux cultes reconnus, décision unilatérale du gouvernement français. Il promulgua l’ensemble par la loi 18 germinal an X (8 avril 1802). Celle-ci comprenait donc d’une part le Concordat, négocié avec Rome, et d’autre part 77 *articles organiques de la convention du 26 messidor an IX*, consacrés au culte catholique auxquels s’ajoutaient 44 *articles organiques des cultes protestants*, visant les Églises réformées et les Églises de la Confession d’Augsbourg. Articles organiques et Concordat ne formaient, pour la France, qu’une seule loi. Pourtant, de son côté, le pape refusa toujours de reconnaitre la moindre autorité aux Articles organiques. Le culte juif sera organisé de façon assez comparable par le décret du 17 mars 1808.

Les Articles organiques édictaient des mesures de :ref:`police` qui organisaient les cultes reconnus en service public, soumis à un strict contrôle de l’État. Les ministres des cultes catholique et protestants étaient salariés de l’État. Ce statut s’accompagnait de mesures de surveillance rigoureuses. Les réunions entre ministres des cultes (art. 4), tout comme les déplacements, étaient soumis à autorisation du gouvernement; les propos tenus pouvaient être sanctionnés; la liberté des manifestations extérieures du culte était strictement encadrée (art. 45). La correspondance avec Rome n’était pas libre (art. 1). Des sanctions étaient prévues en cas de manquement à ces règles : le Conseil d’État pouvait déclarer «l’abus» des agissements contraires à ces dispositions (art. 6, 7, 8). La condamnation était essentiellement d’ordre moral, mais elle pesa pourtant sur la vie religieuse du XIX\ :sup:`e`  siècle. Certes, la célébration publique du culte public était autorisée, mais sévèrement contrôlée. Les Articles organiques furent appliqués avec plus ou moins de rigueur selon les gouvernements et furent notamment souvent invoqués à la fin du XIX\ :sup:`e`  siècle pour justifier la politique anticléricale de la III\ :sup:`e` République. Les Églises réformées devaient s’organiser en consistoires* et synodes; celles de la Confession d’Augsbourg possèderaient des consistoires locaux, des inspections et des synodes généraux. La :ref:`loi du 9 décembre 1905` sur la Séparation des Églises et de l’État n’étant pas applicable dans les trois départements de l’Alsace et de la Moselle*, les Articles organiques y demeurent en vigueur, avec quelques aménagements néanmoins.

Approche patrimoniale
=====================

Les Articles organiques détaillent, plus que ne le fait le Concordat, certaines questions patrimoniales, tout en laissant dans l’ombre d’importants sujets. Il n’en reste pas moins qu’ils participent du régime — dont le :ref:`financement` — des biens des cultes reconnus dont beaucoup, à partir des années 1830 et l’avènement de l’administration des Beaux-arts, seront aussi des monuments historiques.

Les traitements des archevêques, évêques, curés de première ou de seconde classe sont fixés par la loi et peuvent être augmentés par les conseils généraux (art. 64, 65, 66). Vicaires et desservants continueront à jouir des pensions prévues par l’Assemblée constituante (art. 68). Les traitements des pasteurs protestants sont prévus par l’article 7 des Articles organiques des cultes protestants et les rabbins deviendront salariés de l’État par une loi de 1831.

Les départements sont « autorisés » à procurer un logement aux évêques et archevêques (art. 71). Les presbytères et jardins attenants non aliénés seront « rendus » aux curés et desservant (art. 72), sans qu’il soit précisé s’il s’agit d’en restituer la propriété à l’Église ou seulement la jouissance. À défaut de presbytère existant, les communes sont « autorisées » à procurer un logement et un jardin au curé.

Pour les trois cultes, des oblations faites par les :ref:`fidèles` étaient autorisées, notamment pour l’administration des sacrements ou autres célébrations religieuses ; leur montant, fixé par l’évêque, était alors imputé sur la rémunération prévue du ministre du culte (art. 68 des Articles organiques du culte catholique et art. 7 des Articles organiques des cultes protestants). Les fondations étaient également autorisées et devaient avoir pour objet « l’entretien des ministres et l’exercice du culte et ne pourront consister qu’en rentes constituées sur l’État… » ; elles étaient soumises à une étroite tutelle de la part de l’État (art. 73 des Articles organiques du culte catholique et art. 8 des Articles organiques des cultes protestants).

La condition des édifices du culte était également envisagée, mais cette question fondamentale était traitée avec, là encore, quelques imprécisions : « Les édifices anciennement destinés au culte catholique, actuellement dans les mains de la nation, à raison d’un édifice par cure et par succursale, seront mis à la disposition des évêques par arrêtés du préfet du département » (Art. 75). Cures et succursales correspondaient à ce que les fidèles, en accord avec l’Église, appelaient « paroisses », mais le droit français distinguait entre la cure — desservie par un curé ayant le statut et la rémunération correspondants à ce titre — et la succursale, dont un prêtre seulement qualifié de desservant assurait la gestion en percevant un traitement moindre et dont le versement n’apparaissait pas aussi contraignant pour l’État. L’article 77 ajoutait que « Dans les paroisses où il n’y aura point d’édifice disponible pour le culte, l’évêque se concertera avec le préfet pour la désignation d’un édifice convenable ».

La mise à disposition ne réglait pas la question de la propriété. Celle des églises paroissiales et des presbytères fut un temps disputée entre la commune, la fabrique ou l’État. En l’an XIII, deux avis du Conseil d’État (3 nivôse et 2 pluviôse) en attribuèrent la propriété à la commune. Une `ordonnance royale du 31 mars 1837 <https://nakala.fr/10.34847/nkl.23aa2bqo>`_ considère que « les affectations consenties par le concordat et de 1801 et les articles organiques du 18 germinal an X » n’ont pas modifié le droit de propriété de l’État sur les cathédrales (Affre, p. 287 et s). Ce fut d’ailleurs l’État qui, tout au long du régime des cultes reconnus, eut la charge financière de l’:ref:`entretien` et de la réparation des cathédrales et palais épiscopaux, en cas de revenus insuffisants des fabriques.

Quant à la « mise à disposition » des évêques, elle fut entendue comme une mise à disposition pour l’exercice du culte, régime d’affectation cultuelle, liant le propriétaire. Cette conception permettra à l’administration, comme aux tribunaux, la reconnaissance du régime de domanialité publique aux édifices du culte avec une affectation spéciale, contraignante à l’égard de l’État ou de la commune propriétaire. À propos de cette affectation, il était précisé qu’un « même temple ne pourra être consacré qu’à un seul culte » (art. 46) ; la mesure visait à éviter toute querelle entre les religions. Pourtant, les préfets acceptèrent le maintien de la pratique du *simultaneum* qui est encore admis en Alsace-Moselle aujourd’hui. La loi précisait aussi que les immeubles autres que ceux destinés au logement ne seront pas affectés à des titres ecclésiastiques ni possédés par les ministres du culte à raison de leurs fonctions (art. 74).

Toutes ces dispositions visaient à ce que les dépenses mises à la charge de l’État demeurent modestes et, autre objectif allant dans le même sens, à ce que l’Église catholique ne puisse pas reconstituer un important patrimoine. Surtout, la gestion de ce patrimoine était placée sous une tutelle étroite de l’État.

Force est de reconnaître que ni le Concordat ni les Articles organiques ne réglaient, de façon précise et détaillée, la condition du patrimoine cultuel : rien sur les biens d’éventuelles communautés monastiques ou de congrégations, pas davantage sur le patrimoine mobilier ou sur les œuvres d’art. Pourtant, ces textes sont le socle sur lequel s’organisera tout le régime des cultes reconnus pendant un siècle. Une législation complémentaire interviendra, mais sur des questions spécifiques et sans remettre en cause les dispositions napoléoniennes. Dans cette législation ultérieure, l’un des textes essentiels, en particulier pour la gestion du patrimoine, fut le décret de 1809 organisant les fabriques, dont la création avait d’ailleurs été prévue par l’article 76 des articles organiques.

**Brigitte Basdevant-Gaudemet**

 

Bibliographie
-------------- 

* *Répertoire Dalloz*, éd. 1853, v° Culte.

* Affre Denis, *Traité de la propriété des biens ecclésiastiques*, Paris, A. Le Clere, 1837.

* Basdevant-Gaudemet Brigitte, « Propriété publique et affectation cultuelle, fondements historiques », dans B. Basdevant-Gaudemet, M. Cornu, J. Fromageau (dir.), *Le patrimoine culturel religieux*, Paris, L’Harmattan, 2006, p. 77-116.

* Leniaud Jean-Michel, *L’Administration des cultes pendant la période concordataire*, Paris, NEL, 1988.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.5304kq9c>`_