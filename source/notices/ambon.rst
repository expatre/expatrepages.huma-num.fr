:orphan:
.. index::
   single: ambon
.. _ambon:


===============
ambon
===============


→ :ref:`aménagements liturgiques` ; :ref:`art sacré/art religieux` ; :ref:`concile` ; :ref:`concile Vatican II` ; :ref:`mobilier liturgique`

Présentation générale
======================

L’ambon également nommé *pulpitium* (estrade) est une sorte de tribune élevée d’où sont proclamés les textes saints. Il est placé dans le chœur de l’église, généralement, du côté gauche.

Dès la fin du IV\ :sup:`e` siècle, ce type de tribune, appelé *analogium* sert aussi pour l’homélie ou la prédication. Les ambons médiévaux sont placés en avant du chœur et ont souvent deux étages : l’étage supérieur d’où le diacre proclame, face au peuple, l’Évangile et un étage inférieur utilisé par le sous-diacre pour lire les épîtres, tourné vers l’:ref:`autel <autel>`. Certains liturgistes médiévaux, comme Honorius d’Autun (v. 1080-v. 1150) indiquent que l’ambon est utilisé aussi pour le chant du graduel et de l’Alléluia.

Après le :ref:`concile de Trente`, la chaire — de son nom complet chaire de vérité — remplace souvent l’ambon sauf dans les églises conventuelles. Charles Borromée (1538-1584), pour son diocèse de Milan, propose deux ambons, l’un plus élevé pour la proclamation de l’Évangile et un autre pour la lecture des épîtres ; il recommande que l’ambon de l’Évangile, placé à droite, ne soit pas trop éloigné de l’autel. La pratique de l’ambon est cependant abandonnée peu à peu au XVIII\ :sup:`e` siècle, les saintes Écritures étant proclamées depuis l’autel en se tournant vers le peuple. Les homélies et sermons se tiennent depuis la chaire de vérité, éloignée du chœur.

Les réformes liturgiques postérieures au :ref:`concile Vatican II` le remettent à l’honneur et il devient un élément du mobilier* du chœur [Fig. 1], souvent créé en même temps que le nouvel autel, notamment dans le cadre de :ref:`commandes` de création artistique, par exemple ceux réalisés par Philippe et Dominique Kaeppelin (cathédrale* Saint-Bénigne de Dijon), d’Arcabas (cathédrale Saint-Pierre de Rennes) et de Goudji (cathédrale Notre-Dame de Chartres). L’ambon peut être habillé d’un voile de la couleur liturgique du jour ou d’un tissu d’or. Pour la liturgie papale, un ambon amovible a été créé pour se conformer aux prescriptions post-conciliaires. 

**Bernard Berthod**


`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.560e96l7>`_





