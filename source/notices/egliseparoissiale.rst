:orphan:
.. index::
   single: église paroissiale
.. _église paroissiale:


===================
église paroissiale
===================


→ :ref:`curé` ; :ref:`diocèse` ; :ref:`édifice religieux` ; :ref:`fidèles` ; :ref:`paroisse`

Approches religieuse et juridique
=================================

L’église paroissiale est l’édifice désigné comme lieu habituel de rassemblement cultuel de la :ref:`paroisse`. Le canon 1214 du code de :ref:`droit canonique` décrit le bâtiment église comme « l’édifice sacré destiné au culte divin où les :ref:`fidèles` ont le droit d’entrer pour l’exercice du culte divin, surtout s’il est public ». Cet édifice, auquel un titre est attribué (canon 1218), est un lieu sacré, car « destiné au culte divin » (canon 1205) par une dédicace ou au moins une bénédiction (canon 1217 §1) donnée par l’évêque diocésain, dont le consentement formel est d’ailleurs requis pour toute construction d’église (canon 1215 §1). Du fait de la dédicace ou de la bénédiction, « tous les actes du culte divin peuvent être célébrés » dans cette église, « restant saufs les droits paroissiaux » (canon 1219). Ces derniers concernent l’exercice par le curé de la charge pastorale de la paroisse, communauté eucharistique, et « dernier lieu de l’organisation de l’Église » selon l’expression de Jean-Paul II (*Christifidèles* *laici* n° 26).

En conséquence, si toute église doit être dotée d’un :ref:`autel` fixe (canon 1235 §2), les fonts baptismaux reviennent à l’église paroissiale (canon 858 §1) qui d’ailleurs doit être dédicacée ainsi que l’autel selon le rite solennel (canon 1217 §2). Parmi les fonctions spécialement confiées au curé, énumérées au canon 530, l’administration du baptême, l’assistance aux mariages, la célébration des funérailles, la bénédiction des fonts baptismaux au temps de Pâques, la célébration eucharistique plus solennelle le dimanche et les jours de fête d’obligation ont lieu habituellement dans l’église paroissiale. C’est en effet dans cet édifice que les fidèles viennent se rassembler pour recevoir les sacrements et vivre les grandes étapes de leurs vies.

Le statut juridique de ces :ref:`édifices religieux` en droit français est à considérer quant au maintien et à l’utilisation des églises paroissiales, qui forment une part si significative — et sans doute la plus emblématique — du patrimoine culturel religieux [Fig. 2]. Selon un état des lieux publié en 2016 par la Conférence des évêques de France, sur un total de 42 258 églises et chapelles paroissiales, 1886 églises ont été construites depuis la :ref:`loi du 9 décembre 1905` concernant la séparation des Églises et de l’État par les associations diocésaines. Les autres églises paroissiales sont la propriété de la commune (et de l’État pour les cathédrales). Cette :ref:`propriété` des collectivités publiques est grevée d’une :ref:`affectation cultuelle` en vertu de l’article 5 de la loi du 2 janvier 1907, qui met les édifices et leur mobilier « à disposition des fidèles et des ministres du culte pour la pratique de leur religion. » Il ne peut être mis fin à la jouissance de ces biens qu’après leur :ref:`désaffectation <désaffectation cultuelle>` prononcée dans les conditions prévues par l’article 13 de la loi du 9 décembre 1905, notamment « si en dehors des cas de force majeure, le culte cesse d’être célébré pendant plus de six mois consécutifs ». Or, ces dispositions toujours en vigueur s’appliquent à un contexte sociologique contemporain fort différent : toutes les églises paroissiales ne connaissent plus une fréquentation quotidienne et/ou hebdomadaire systématique.

Parmi les mutations en cours de la paroisse figure l’extension de son territoire. Aussi, dans les nouvelles paroisses constituées par suppression canonique des paroisses anciennes, qualifiées de « relais », les évêques, après avoir déterminé quelle sera l’église dite principale pour rassembler la communauté à l’occasion de l’eucharistie dominicale, prennent soin de préciser lors de la rédaction des décrets d’érection que les églises des anciennes paroisses demeurent des « églises paroissiales ». Ainsi, le curé reste affectataire de toutes et chacune des églises existant sur le territoire de la paroisse. Le maintien d’une activité cultuelle tient à des raisons pastorales, mais aussi au régime de la domanialité publique, du fait de la tentation de certaines municipalités propriétaires d’églises communales, lassées d’assurer leur :ref:`entretien <entretien/réparations>`, de se tourner vers une procédure de désaffectation (même si l’:ref:`accord préalable <accord préalable de l'affectataire cultuel>` du curé — en communion avec sa :ref:`hiérarchie` — est nécessaire pour qu’une telle procédure aboutisse). La survie d’une communauté chrétienne de proximité dans les relais implique de maintenir l’église ouverte pour des temps de prière. De plus mariages, baptêmes et funérailles y demeurent célébrés pour les personnes ayant leur domicile dans la commune.

Les dépenses d’entretien et de conservation des édifices cultuels qui font partie du domaine public de la commune amènent les municipalités à soutenir des demandes d’utilisation à caractère culturel de ces églises d’autant plus si elles sont peu utilisées par la communauté chrétienne locale, voire souvent fermées notamment par crainte des :ref:`vols <vol>`. C’est au prêtre, légitimement nommé curé de la paroisse à qui il revient d’apprécier si une activité autre que cultuelle n’est pas contraire à la « sainteté du lieu » (canon 1210) avant de donner une autorisation écrite. Il s’agit pour faire vivre autrement ce patrimoine religieux de concilier les droits du propriétaire avec ceux de l’affectataire de l’édifice.

**Élisabeth Abbal**

Bibliographie
--------------

* Secrétariat de la Conférence des Évêques de France, Comité national d’art sacré, *Les églises communales. Textes juridiques et Guide pratique*, Paris, Cerf, 1995.

* Habert Jacques (dir.), « Ces églises qui font l’Église. Les églises, un enjeu pour tous », Actes du colloque tenu le 10 mars 2017 au collège des Bernardins, dans *Documents Épiscopat*, n 6/7, 2017.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.cd385d31>`_



