:orphan:
.. index::
   single: exécration
.. _exécration:


===============
exécration
===============


→ :ref:`désaffectation cultuelle` ; :ref:`édifice religieux` ; :ref:`église paroissiale` ; :ref:`patrimoine funéraire`

Approche religieuse
===================

Le terme d’exécration appartient au vocabulaire liturgique et canonique ancien. Dans le Code de droit canonique de 1917, il n’apparaissait pas, alors qu’il était employé dans les commentaires pour désigner ce que le canon 1170 de ce Code appelle la perte de consécration ou de bénédiction d’une église. Bien qu’il soit encore d’usage courant, il n’est pas non plus utilisé dans le Code de droit canonique de 1983. Celui-ci reprend les expressions du Code de 1917 et les applique non seulement aux églises mais aussi aux autres « lieux sacrés », soit, selon la qualification utilisée dans la législation ecclésiastique actuelle, les oratoires et :ref:`chapelles <chapelle>` privées, les :ref:`sanctuaires <sanctuaire>`, les :ref:`autels <autel>` et les cimetières (can. 1205). Tous ces lieux, auxquels le Code consacre un ensemble de canons, sont destinés au culte divin ou à la sépulture des :ref:`fidèles`. Leur consécration est due à un acte liturgique qui leur confère un caractère sacré, la dédicace par l’évêque diocésain ou la bénédiction accomplie par un de ses vicaires selon un rite décrit dans le *Pontifical romain* à l’usage des évêques. Le canon 1212 indique les motifs pour lesquels un lieu peut perdre son caractère sacré. Il peut être devenu inutilisable parce que la plus grande partie en a été détruite, ou avoir été réduit à des usages profanes mais, dit le Code, « de manière permanente ». Dans ces deux cas, l’exécration doit être prononcée par un décret de l’autorité ecclésiastique, qui décide la « perte de dédicace ou de bénédiction ». L’exécration peut donc être la conséquence d’une situation « de fait » ; ainsi de cimetières abandonnés, ou d’églises, chapelles ou autels qui ne servent plus depuis longtemps au culte et sont réduits à des usages non liturgiques mais un décret de l’autorité ecclésiastique constaterait cet état de fait et informerait du statut du lieu.

En revanche, le cas désormais fréquent en France d’églises paroissiales ne servant plus au culte – ou que très rarement – dans une communauté érigée en :ref:`paroisse` ne relève pas du canon 1212. En effet, il ne s’agit pas là d’une réduction de fait à un usage profane. C’est ici le canon 1222 qui s’applique et uniquement aux églises, parmi les lieux sacrés. Il traite explicitement des motifs permettant à l’autorité ecclésiastique de décider de la perte du caractère sacré des églises. Il restreint cette perte à des cas précis et fait une distinction entre la situation matérielle du bâtiment et les difficultés touchant à son usage cultuel.

Un premier paragraphe décide que, si une église ne peut « en aucune manière servir au culte divin » en raison de son mauvais état et de l’impossibilité d’y remédier, elle peut être réduite par l’évêque diocésain lui-même à un usage profane qui ne soit pas inconvenant. Selon le canon, la destination du lieu après son exécration est un élément du décret épiscopal, sans toutefois que l’autorité ecclésiastique puisse exiger que, par la suite, cette destination soit respectée. Un second paragraphe donne un pouvoir de décision étendu à l’évêque diocésain, à qui revient de juger discrétionnairement de l’opportunité d’une exécration. Si, dit le canon, « d’autres causes graves » conseillent qu’une église ne serve plus au culte divin, l’évêque diocésain peut décider qu’elle perde sa dédicace ou sa bénédiction. Les causes graves ne sont pas décrites par le Code. L’une d’entre elles pourrait être celle que nous évoquions plus haut pour la France, à savoir l’absence d’un nombre suffisant de fidèles ou une telle pénurie de prêtres que l’usage du lieu devienne impossible. Une autre cause pourrait être un nombre trop grand d’églises à entretenir à l’intérieur d’un :ref:`diocèse`. Étant donné que la décision d’exécration d’une église relève d’une libre appréciation de l’évêque et vu l’importance d’une telle décision pour les fidèles qui la fréquentent, en particulier quand celle-ci a le statut d’église paroissiale, la législation canonique impose une procédure de consultation et donne des éléments de principe à respecter.

D’abord, l’évêque diocésain devra entendre le conseil presbytéral, c’est-à-dire la réunion des prêtres formant le « sénat » de l’évêque (can. 495), et recevoir son avis qui ne le lie pas. Dans la pratique, l’évêque diocésain recueille aussi l’avis du curé de la paroisse si l’église est paroissiale. Ensuite, l’évêque est tenu de recueillir le consentement « de ceux qui revendiquent légitimement leurs droits sur cette église ». Cette règle est générale. Son application dépend du statut de l’église, qui peut appartenir au diocèse ou à une personne juridique avec d’éventuelles dispositions conventionnelles liant l’:ref:`affectataire cultuel` à ceux qui ont contribué à sa construction. En France, nombre d’édifices affectés au culte catholique appartiennent à l’État et surtout aux communes. En conséquence la procédure de :ref:`désaffectation cultuelle` prévue dans l’article 13 de la :ref:`loi du 9 décembre 1905` et le décret du 17 mars 1970 s’applique à eux. Parmi les cinq motifs prévus par la loi, deux rejoignent d’ailleurs ceux établis par le :ref:`droit canonique` : en dehors des cas de force majeure, le culte peut avoir cessé d’être célébré pendant plus de six mois consécutifs ou la conservation de l’édifice ou du mobilier classé – au titre des monuments historiques – qu’il contient être compromise par insuffisance d’:ref:`entretien <entretien/réparations>`. La procédure prévue par le décret de 1970 est –dans de très rares cas au demeurant – mise en œuvre lorsqu’un édifice cultuel n’est plus utilisé, l’arrêté préfectoral prononçant la désaffectation étant obligatoirement précédé du contentement de l’évêque diocésain.

Enfin, le Code de droit canonique affirme un principe dont l’application et l’appréciation dépendent de l’évêque, au moins depuis sa promulgation en 1983 (sous l’empire du Code précédent, tout évêque devait s’adresser à la Curie romaine) : en cas d’exécration, le « bien des âmes » ne doit subir aucun dommage. Cette règle conduit à donner aux personnes concernées un droit d’expression de leur volonté, donc de leur opposition à une éventuelle décision d’exécration. Elle fonde un intérêt légitime à agir qui a amené des fidèles de divers pays, en particulier les États-Unis où des ventes d’églises ont été décidées dans de nombreux diocèses, à former des recours devant la Congrégation pour le clergé de la Curie romaine et le Tribunal suprême de la Signature apostolique à Rome, qui juge la légitimité des actes émanant de l’exercice du pouvoir administratif ecclésiastique (can. 1732). Une fois encore, le canon 1222 § 2 mentionne que l’exécration de l’église pour des causes graves ne peut réduire celle-ci à un usage profane qui soit inconvenant.                                                                                            

**Patrick Valdrini**

Bibliographie
--------------

* Longhitano Adolfo, « Execración », *Diccionario general de Derecho canónico*, Vol. III, Cizur Menor, Aranzadi, 2012.





`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.dce37mj3>`_