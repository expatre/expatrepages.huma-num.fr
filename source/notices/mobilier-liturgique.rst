:orphan:
.. index::
   single: mobilier liturgique
.. _mobilier liturgique:


====================
mobilier liturgique
====================


→ :ref:`affectation cultuelle` ; :ref:`aménagement liturgique` ; :ref:`décor` ; :ref:`meuble/immeuble` ; :ref:`sièges`


Approches historique et religieuse
===================================

Le mobilier liturgique meuble les :ref:`édifices religieux <édifice religieux>`. Au-delà des variations de dénomination selon les traditions religieuses, son but est principalement utilitaire tout comme les :ref:`objets liturgiques <objet liturgique>` : permettre la célébration publique du :ref:`culte`.

Le meuble principal est l’:ref:`autel` qui accueille le sacrifice, puis l’:ref:`ambon` où est proclamée la Parole de Dieu, la :ref:`chaire` [Fig. 1] de vérité d’où le clerc explique ladite Parole, le banc de communion où les :ref:`fidèles` reçoivent l’Eucharistie, le confessionnal où ils confessent leurs fautes et reçoivent l’absolution, et les fonts baptismaux. D’autres meubles sont plus accessoires bien qu’utiles comme les :ref:`sièges`, les prie-Dieu, le jubé ou la barrière de chœur qui séparait le clergé des laïcs ; la crédence qui reçoit les objets liturgiques. D’autres sont purement décoratifs comme les retables d’autel. À la différence de l’autel, aucun de ces meubles ne reçoit de bénédiction.

Le confessionnal [Fig. 3] est un meuble destiné à recevoir les confessions des fidèles et l’absolution de leurs fautes, mais il n’est pas obligatoire pour que s’exerce le sacrement de pénitence. Il apparaît dans les églises au cours du XVI\ :sup:`e` siècle et semble avoir été institué par saint Charles Borromée. Le concile provincial de Milan de 1565 décide qu’une tablette doit séparer le confesseur du pénitent. Cette disposition gagne ensuite l’Italie puis la *Propaganda Fide* étend l’obligation du confessionnal à tous les territoires de mission. Au cours du XVII\ :sup:`e` siècle, les confessionnaux à une place accueillent une deuxième place, en positionnant le confesseur au milieu. Le confessionnal moderne est muni d’une grille qui sépare le confesseur du pénitent agenouillé.

La crédence [Fig. 4] est une table de décharge qui permet de poser tout le matériel utilisé lors des offices liturgiques. On distingue la crédence épiscopale, qui reçoit les insignes pontificaux, la crédence de l’office solennel, celle des messes basses et celle des offices extraordinaires. Elle est placée du côté de l’épître et garnie d’une housse en toile blanche.

Le banc de communion (dit aussi agenouilloir de communion : Vergain, Duhau, p. 4) [Fig. 5] consiste en une table basse en bois, en ferronnerie ou en marbre contre laquelle les fidèles agenouillés viennent s’appuyer pour recevoir la communion. Ce meuble apparaît lors de la démolition des jubés pour limiter l’accès au chœur ; une porte est ménagée en son centre pour en permettre l’accès. Au XIX\ :sup:`e` siècle, la partie horizontale est souvent recouverte d’une nappe en lin pour pousser la comparaison symbolique avec la table simple. Au XX\ :sup:`e` siècle, la dénomination courante devient : table de communion.

Les fonts baptismaux [Fig. 6] se présentent sous la forme d’un bassin — ou d’une cuve — placé sur un socle, utilisé pour l’administration du baptême. Dans les premiers siècles de l’Église, les catéchumènes étaient entièrement immergés dans le baptistère de la cathédrale, construction en forme de bassin creusé à même le sol. Après le VIII\ :sup:`e` siècle, les volumes se réduisent, le bassin devient une vasque qui peut recevoir un couvercle ouvragé. Selon le *Rituel romain*, chaque :ref:`église paroissiale` doit posséder des fonts placés dans un endroit convenable de l’église ; de fait, il est souvent édifié dans une chapelle latérale proche de la porte. Ils contiennent une eau préalablement bénite. Le matin du Samedi saint, veille de Pâques, les fonts sont vidés et nettoyés.

Le retable d’autel [Fig. 7], élément architectural plus ou moins volumineux est, comme son nom l’indique (*retro tabula*), placé derrière l’autel. Il apparaît à la fin du XIII\ :sup:`e` siècle et devient un lieu d’expression artistique et apologétique permettant d’exposer la vie d’un saint. Les premiers retables sous forme de triptyque ou de polyptyques sont souvent amovibles et posés sur une estrade fixée à l’autel, la prédelle. Il est réalisé en pierre ou en bois présentant des scènes sculptées ou des peintures.

Le `jubé <https://nakala.fr/10.34847/nkl.eb74t40a>`_ [Fig. 8] est une séparation élevée entre le chœur et la nef de l’église ; souvent il circonscrit entièrement le chœur. À l’époque carolingienne, c’est une sorte de galerie suspendue d’où intervient le lecteur ou le diacre qui semble succéder au cancel. Son nom « provient de la phrase *Jube domine benedicere* par laquelle le clerc sollicite la bénédiction de l’évêque ou du prêtre. » (Leclercq, col. 2767). Cette construction qui se transforme en mur de séparation apparaît au XIII\ :sup:`e` siècle. Certains archéologues y voient une combinaison de la poutre du chœur soutenant un crucifix et une cloison héritée des Églises d’orient. Le jubé s’ouvre sur la nef par une porte et deux autels latéraux prennent appui sur le mur antérieur permettant aux fidèles d’assister à la célébration liturgique. La plupart des jubés et surtout le mur antérieur sont détruits après le concile de Trente pour donner une meilleure visibilité aux célébrations d’autant que les chanoines et les clercs astreints au chœur aspirent à moins d’enfermement. À partir du XVII\ :sup:`e` siècle, la maçonnerie aveugle du jubé est remplacée par des grilles ouvragées qui permettent le regard tout en conservant une séparation virtuelle du clergé et du peuple.

**Bernard Berthod**



Bibliographie
--------------

* Bouvet, Mireille-Bénédicte, *Protestantismes. Vocabulaire typologique*, Paris, Éditions du patrimoine, 2017.
* Cabrol Fernand, Leclercq Henri, « Jubé », *Dictionnaire d’Archéologie chrétienne et de Liturgie*, t. VII, Paris, Letouzey et Ané, 1927, col. 2767-2769.
* Finance, Laurence de, « L’inventaire des objets mobiliers religieux des XIX\ :sup:`e` et XX\ :sup:`e` siècles : méthodologie », *In Situ*, 11, 2009 : http://journals.openedition.org/insitu/4468.
* Le Vavasseur Léon-Michel, *Cérémonial de la consécration des Églises et des autels et de la bénédiction d’un cimetière et d’une cloche*, Paris, Lecoffre, 1865.
* Martimort A.-G., « La disposition de l’autel », *L’Art sacré*, Paris, 1950, n° 7-8, p. 19-21.
* Servières Georges, « Les jubés (origine, architecture, décoration, démolition) (Premier article) », *Gazette des beaux-arts*, t.60, 1918, p. 355-380.
* ergain Philippe, Duhau Isabelle (dir.), *Thésaurus de la désignation des objets mobiliers*, Inventaire général du patrimoine culturel — Ministère de la Culture et de la Communication, 2014.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.2a51c339>`_



