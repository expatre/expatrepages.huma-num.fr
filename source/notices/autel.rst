:orphan:
.. index::
   single: autel
.. _autel:


===============
autel
===============


→ :ref:`concile Vatican II` ; :ref:`église paroissiale` ; :ref:`liturgie` ; :ref:`messe` ; :ref:`relique/reliquaire`

Approches historique et religieuse
===================================

:ref:`Meuble <meuble>` religieux, l’autel apparaît dans différentes traditions religieuses. Il est le point central des lieux de :ref:`culte` chrétiens (il est nommé table de communion dans la tradition calviniste, table « présente principalement dans les :ref:`temples` réformés et les églises presbytériennes », Bouvet, p. 144) : la célébration de la :ref:`messe`, vécue comme un repas commémorant le Jeudi saint, est organisée autour d’une table, appelée autel. La forme de l’autel, ainsi que son usage, ont varié au cours des siècles et sa caractérisation varie suivant son emplacement dans l’:ref:`édifice religieux`, « la fonction liturgique privilégiée (autel du saint sacrement, etc.) et la morphologie (autel-tombeau, etc.) » (Vergain, Duhau, p. 45 ; `Perrin <https://journals.openedition.org/insitu/1049>`_, p. 1 et s.).

À l’origine, c’est une pierre plane soutenue par des colonnes, puis un autel plein formé d’un seul bloc ou maçonné. Aux premiers siècles, la table est en bois puis l’usage de la pierre s’impose au V\ :sup:`e` siècle et devient obligatoire le siècle suivant. Par la suite, diverses obligations apparaissent : la pierre doit être naturelle, dure, compacte et non friable ; la pierre reconstituée est prohibée (règle reprise dans le *Codex Iuris Canonici* de 1917, can. 1198, § 1). Dès le IV\ :sup:`e` siècle, cette table est édifiée sur le tombeau d’un martyr ou d’un saint dont l’église est le mémorial. Avec l’extension du christianisme, il devient de plus en plus difficile d’édifier une église sur le tombeau d’un martyr ; pour conserver cette tradition, une relique est déposée et scellée dans le corps de l’autel ; à cet effet, une cavité est ménagée dans la table pour y insérer la ou les reliques. Du point de vue symbolique, dès le VI\ :sup:`e` siècle, les liturgistes voient dans l’autel une figure du Christ reprise ensuite dans le *Pontifical* et les nappes sont considérées comme les membres du Christ, c’est-à-dire les :ref:`fidèles`. 

Après la réforme tridentine (XVI\ :sup:`e` siècle) et l’apparition des autels surmontés par un retable [Fig. 2], la table de l’autel, souvent en bois, incorpore une pierre en forme de carreau, appelée pierre d’autel ; cette dernière est gravée de cinq :ref:`croix`, dans chaque angle et au centre, et reçoit les onctions de la consécration et sous laquelle est disposée une relique. L’autel moderne se divise en plusieurs parties : le tombeau, le retable et le tabernacle. Au XIX\ :sup:`e` siècle, avec le mouvement néogothique, l’autel prend une grande place dans le :ref:`décor`, il est souvent placé sous un ciborium et entouré de courtines.

Pour la célébration liturgique, l’autel est paré, c’est-à-dire habillé d’étoffes et équipé d’un luminaire. La table reçoit trois nappes disposées l’une sur l’autre et sur le devant est tendue une pièce d’étoffe ouvragée appelée antependium. L’*Ordo romain* dit de Paul VI (1969) n’impose qu’une seule nappe blanche. L’autel est dépouillé, à l’image du Christ, le soir du Jeudi saint. À la suite du :ref:`concile Vatican II`, l’*Ordo* conseille la célébration face aux fidèles ce qui entraîne une modification radicale de la disposition de l’autel et de son décor ; c’est désormais une table munie ou non d’un antependium, sans tabernacle ou quoique ce soit d’élevé pouvant gêner la vision. La pierre d’autel n’est plus obligatoire ; cependant des reliques sont scellées dans l’autel. Pendant les deux ou trois décennies qui ont suivi le décret de 1969, la plupart des églises sont munies d’un autel provisoire, mobile et de facture modeste, n’ayant reçu aucune consécration.

L’autel est consacré par l’ordinaire du lieu, un évêque désigné ou un cardinal, même si ce dernier n’est pas revêtu du caractère épiscopal. La consécration a lieu au cours de la cérémonie de la dédicace par trois séries d’onctions réalisées, les deux premières avec l’huile des catéchumènes, la troisième avec le Saint-Chrême, puis il est encensé et signé avec l’eau grégorienne. Cette consécration peut se faire en dehors de la cérémonie de la dédicace, lors que l’ancien autel a été détruit ou fortement modifié. La consécration épiscopale par l’onction demeure après Vatican II. L’autel fixe perd sa consécration si la table est séparée de sa base, même momentanément, si les reliques sont extraites : on dit alors qu’il est exécré. Pour une nouvelle consécration, l’évêque du lieu peut confier la charge à un prêtre qu’il désigne.Dans une même église, on distingue plusieurs types d’autel : l’autel majeur, les autels latéraux [Fig. 3], les autels privilégiés, etc. L’autel majeur est celui placé dans le chœur de l’édifice et où sont célébrées les cérémonies liturgiques importantes comme les messes paroissiales dominicales. Dans certaines églises et cathédrales, seul l’évêque peut y célébrer ; c’était le cas à Rome pour les autels majeurs des basiliques majeures, où seuls le pape ou son légat pouvaient officier. À partir de la fin du XVI\ :sup:`e` siècle, l’autel majeur est surmonté d’un tabernacle qui contient la réserve eucharistique. Les autels latéraux sont placés dans les :ref:`chapelles <chapelle>` latérales ou dans le transept et permettent des célébrations à caractère plus privées : messe de souvenir, de :ref:`confréries <confrérie>`. Dans les établissements conventuels masculins, la chapelle est garnie de nombreux autels latéraux pour la célébration quotidienne et individuelle de la messe. Cette disposition a disparu après Vatican II. Tout comme l’église dans lequel il est installé, l’autel a un titulaire, l’autel majeur ayant le même titulaire que l’édifice. Les autels latéraux sont dédiés à des saints locaux, à des saints protecteurs de confréries, etc. Le Saint-Siège peut concéder, par un indult (dérogation), la titulature d’un bienheureux honoré localement. L’autel privilégié est un autel enrichi, par concession du Saint-Siège, d’une indulgence plénière applicable aux âmes des défunts à l’intention desquelles la messe y est célébrée. Cette concession est généralement donnée à perpétuité. La tradition attribue au pape Grégoire-le-Grand (590-604) l’introduction de cet usage. Les fidèles sont informés par une plaque apposée près de l’autel avec les mots *ALTARE PRIVILEGIARUM PRO DEFUCTIS* (décret du 13 août 1667). Cette attribution disparait après le concile Vatican II.

Un autel portatif [Fig. 4] est utilisé par les prélats, évêques et abbés, puis les cardinaux lors de leur déplacement. L’autel mobile se présente comme une pierre d’une trentaine de centimètres de côté et de trois à cinq centimètres d’épaisseur, généralement enchâssée dans du métal précieux. Son usage est connu dès le III\ :sup:`e` siècle, Cyprien de Carthage (v. 200-258) le recommande aux prêtres qui vont célébrer dans les prisons. Les évêques missionnaires des terres barbares aux VII\ :sup:`e` et VIII\ :sup:`e` siècles, comme saint Wulfran de Sens (v. 640-720), apôtre de la Frise, l’utilisent. Les premiers connus datent du VII\ :sup:`e` siècle comme celui de saint Cuthbert (v. 635-687) conservé à la cathédrale de Durham. L’usage de l’autel portatif devient un privilège concédé par le pape à partir du XIII\ :sup:`e` siècle. Dès l’ère carolingienne, les autels portatifs doivent répondre aux mêmes exigences canoniques que les autels maçonnés : être en pierre et avoir été consacrés par un évêque. La pierre souvent précieuse doit être d’un seul tenant, car elle représente le Christ, « pierre rejetée par les bâtisseurs et devenue pierre angulaire » (Mt 21:42) et ne doit pas porter d’inscription ou de décor. Les aumôniers scouts en ont reçu le droit par indult le 15 juillet 1929, puis ceux du Secours Catholique, le 12 mai 1947.

À l’instar de l’:ref:`ambon`, sinon davantage, l’autel occupe une place fondamentale dans le :ref:`mobilier liturgique` et se trouve de ce fait au premier plan dans les programmes de création en :ref:`art sacré/art religieux`.

**Bernard Berthod**

Bibliographie
--------------

* Bouvet, Mireille-Bénédicte, *Protestantismes. Vocabulaire typologique*, Paris, Éditions du patrimoine, 2017.

* Naz Robert (dir.), *Dictionnaire de Droit canonique*, Letouzey et Ané, Paris, T. 1, col. 1456-1468.

* Perrin Joël, « L’autel : fonctions, formes et éléments », *In Situ*, n° 1, 2001. En ligne : `http://journals.openedition.org/insitu/1049 <http://journals.openedition.org/insitu/1049>`_

* Le Vavasseur Léon-Michel, *Cérémonial de la consécration des églises et des autels et de la bénédiction d’un cimetière et d’une cloche*, Paris, V. Lecoffre, 1865.

* Vergain Philippe, Duhau Isabelle (dir.), *Thésaurus de la désignation des objets mobiliers*, Inventaire général du patrimoine culturel — Ministère de la Culture et de la Communication, 2014.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.98fe940b>`_





