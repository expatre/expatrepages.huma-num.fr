:orphan:
.. index::
   single: profanation/sacrilège
.. _profanation/sacrilège:


========================
Profanation / Sacrilège
========================


→ :ref:`droit pénal` ; :ref:`exécration` ; :ref:`édifice religieux` ; :ref:`mobilier liturgique` ; :ref:`objet liturgique`

Approche religieuse
====================

La notion canonique de profanation est liée au respect du caractère sacré qu’un lieu ou une chose a reçu officiellement au cours d’un acte liturgique rituel accompli par l’évêque diocésain ou un autre :ref:`clerc`. Pour le :ref:`droit canonique`, les lieux sacrés sont les églises, les oratoires, les :ref:`chapelles <chapelle>`, les :ref:`sanctuaires <sanctuaire>`, les :ref:`autels <autel>` et les cimetières. Leur caractère sacré leur est conféré par une dédicace ou une bénédiction, soit deux cérémonies distinctes par la solennité et le statut du ministre qui l’accomplit. Ils peuvent le perdre par une procédure régulière d’:ref:`exécration` pour des motifs et dans des conditions prévues par le Code de droit canonique. De la même manière, les objets sacrés, appelés dans le Code « choses sacrées », sont destinés au culte divin par bénédiction : calices, crucifix, tabernacles, :ref:`ambon`, vêtements liturgiques, etc. Leur destination peut leur être retirée par un décret de l’autorité ecclésiastique compétente.

Le Code de droit canonique de 1983 consacre un canon à la profanation des lieux sacrés (can. 1211). Alors que la législation précédente énumérait les actes posés dans une église qui, pourvu qu’ils fussent certains et notoires, permettaient de dire que celle-ci était profanée (can. 1172), le Code actuel étend l’application du canon aux autres lieux sacrés et s’en tient à mentionner les conditions des actes commis qui, au jugement de l’autorité ecclésiastique, entraîneraient la profanation. En premier lieu, les actions doivent être « gravement injurieuses » et avoir entraîné le scandale des :ref:`fidèles`. En second lieu, les actes commis doivent être « si graves et contraires à la sainteté du lieu qu’il ne soit pas permis d’y célébrer le :ref:`culte` tant que l’injure n’a pas été réparée ». Ces critères permettent à l’autorité ecclésiastique du :ref:`diocèse`, l’évêque diocésain ou ses vicaires, de qualifier juridiquement les actions d’actes profanatoires et d’envisager la procédure de réparation pénitentielle prévue dans les livres liturgiques, procédure nécessaire pour que le culte puisse être de nouveau célébré dans l’église. Il leur revient aussi d’évaluer l’imputabilité de l’acte à son auteur s’il est un fidèle de l’Église catholique. L’acte pourrait être un délit au regard du droit canonique et entraîner une sanction spirituelle ou matérielle (canons 1375-1376). Pour cela il faudrait que, étant une violation de la loi ecclésiastique qui impose le respect du caractère sacré d’un lieu, il ait été commis par dol (violation délibérée de la loi) ou faute (négligence coupable).

Le Code envisage aussi la profanation des choses sacrées. Le canon 1171 dispose qu’elles doivent être traitées avec respect et non employées à un usage profane ou impropre. Cette règle générale s’applique lorsque les choses appartiennent à l’Église, ainsi les :ref:`objets liturgiques <objet liturgique>` d’une :ref:`paroisse`, et lorsqu’elles sont la :ref:`propriété` de personnes privées, à condition qu’elles aient été bénies. L’affirmation de cette règle est renforcée par un canon qui interdit que de telles choses soient utilisées pour des usages profanes (can. 1269). Si l’autorité ecclésiastique considérait comme un délit l’usage d’un objet sacré non respectueux de son caractère, l’imposition d’une juste peine pourrait être envisagée, à l’image de celle prévue pour la profanation d’un lieu sacré. Enfin, la législation traite d’une manière spéciale la profanation des espèces sacrées, c’est-à-dire les hosties consacrées au cours d’une célébration eucharistique. Elles sont conservées habituellement dans un tabernacle dont le Code exige qu’il soit inamovible, fait d’un matériau solide, non transparent et fermé, « de telle sorte que soit évité au maximum tout risque de profanation » (can. 938 § 3). Cette dernière est considérée par le Code de droit canonique comme un délit majeur, car elle entraîne une excommunication *latae sententiae* réservée au Siège apostolique, interdisant entre autres de célébrer et de recevoir les sacrements (can. 1367). Un clerc ayant commis de tels actes recevrait d’autres peines jusqu’au renvoi de l’état clérical. C’est dans ce canon et la seule fois dans le Code qu’apparaît le terme de *sacrilège*, qualifiant l’acte intentionnel de recel des espèces consacrées à des fins irrévérencieuses.

**Patrick Valdrini**

Bibliographie
--------------

* Borras Alphonse, V° « Profanacíón », dans *Diccionario general de Derecho canónico*, Vol. VI, Cizur Menor, 2012.

* Ghesquières L.-É., « Profanation », dans *Catholicisme, hier, aujourd’hui, demain*, Vol. XI, Paris, Letouzey & Ané, 1988.
 

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.b5a81h2z>`_





