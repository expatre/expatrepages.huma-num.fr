:orphan:
.. index::
   single: departement des édifices cultuels
.. _departement des édifices cultuels:


===================================================
département des édifices cultuels et historiques
===================================================


→ :ref:`loi du 9 décembre 1905` ; :ref:`conservation des Oeuvres d'Art Religieuses et Civiles` ; :ref:`maîtrise d'ouvrage/d’œuvre` ; :ref:`restauration` ; :ref:`valorisation`

Approches historique et patrimoniale
====================================

La Ville de Paris est actuellement :ref:`propriétaire <propriété>` de 96 :ref:`édifices religieux <édifice religieux>`. Ce nombre, à lui seul, explique et justifie l’existence, au sein des services municipaux parisiens, d’une entité dédiée à cet ensemble patrimonial spécifique et prestigieux : le Département des édifices cultuels et historiques (DECH), service intégré à la sous-direction du Patrimoine et de l’histoire de la direction des Affaires culturelles.

À ce jour, 66 de ces édifices sont l’objet d’une protection au titre des :ref:`monuments historiques` (51 classés, 15 inscrits), 26 sont protégés dans le cadre du Plan Local d’Urbanisme, 3 ont été distingués par le :ref:`label Architecture contemporaine remarquable`. À de rares exceptions — des petites églises des communes annexées à Paris en 1860 ou des :ref:`chapelles <chapelle>` — ces édifices sont de très grandes dimensions. Saint-Sulpice, Saint-Eustache, La Madeleine, le Sacré-Cœur, Saint-Roch, notamment, ont des dimensions de :ref:`cathédrales <cathédrale>`. La majorité des édifices date d’avant 1905 et se situe dans les dix premiers arrondissements formant le centre ancien de Paris. Toutefois, douze édifices du XX\ :sup:`e` siècle sont répartis dans neuf arrondissements périphériques. Pour la plupart, bien que postérieurs à la :ref:`loi du 9 décembre 1905`, ils sont affectés au culte du fait qu’ils se sont substitués à des églises antérieures à la Séparation, démolies pour cause de vétusté ou dans le cadre d’une rénovation urbaine. Sur les 96 édifices, 85 sont :ref:`affectés au culte <affectation cultuelle>` catholique, 9 au culte protestant, 2 au culte israélite. Enfin, ces édifices conservent 130 :ref:`orgues <orgue>`, la plus riche collection au monde aux mains d’un seul et même propriétaire, également à la charge du DECH.


L’histoire du DECH commence au début du XIX\ :sup:`e` siècle. 1822 est une date clé : un bureau des Cultes et des Beaux-arts est créé, associant au sein des services de la préfecture de la Seine l’enjeu patrimonial constitué par les édifices cultuels à entretenir, agrandir, construire ou enrichir d’œuvres par la :ref:`commande publique <commande/commanditaire>` et l’organisation des :ref:`cultes <culte>`. De ce moment, les édifices cultuels appartenant à la ville de Paris sont un ensemble patrimonial spécifique, traité comme tel par un service municipal dédié. Périodiquement réformé ou déplacé dans l’organigramme en fonction du contexte politique, ce bureau voit son organisation adaptée à des ambitions de plus en plus importantes tandis qu’en parallèle se développe, à l’échelle nationale, le service des Monuments historiques.

La loi du 9 décembre 1905 séparant les Églises et l’État — et les lois de 1907 et 1908 — bouleverse les missions attribuées aux services et la teneur de l’ensemble patrimonial cultuel relevant de la collectivité. Elle se traduit à Paris par une récupération presque complète de leurs édifices par les cultes protestant et israélite tandis que la plupart des églises et chapelles affectées au culte catholique demeurent propriétés de la Ville. Après 1905, une inspection des édifices religieux est intégrée dans le service des beaux-arts, lequel est érigé soit en direction à part entière, soit en sous-direction dans une direction plus vaste. C’est ainsi qu’en 1960, les missions d’:ref:`entretien <entretien/réparations>` et de grosses réparations des édifices religieux, de même que l’inventaire et la conservation des œuvres, sont attribuées à une direction célébrant la société des loisirs sous le nom de direction des Beaux-arts, de la jeunesse et des sports. Cette situation perdure jusqu’à la création, en 1978, de la direction des Affaires culturelles au sein des services de la ville de Paris. La localisation administrative des services dédiés au patrimoine cultuel de la Ville se stabilise alors au sein d’une sous-direction *in fine* dénommée « du Patrimoine et de l’histoire ». Tout d’abord bureau « des Monuments » chargé des travaux sur les édifices cultuels et les orgues, puis « des Affaires cultuelles et historiques » et en 2003 « des Édifices cultuels et historiques », l’actuel DECH est l’aboutissement d’une double évolution : vers le plein exercice de la :ref:`maîtrise d’ouvrage <maîtrise d'ouvrage/d’œuvre>` publique des travaux nécessaires à la pérennisation de ces édifices et vers la mission de représentant de la ville de Paris, propriétaire, auprès des cultes affectataires. Le statut juridique de ces édifices les différencie en effet des autres propriétés immobilières municipales (équipements de service public ou domaine privé encadré par des baux locatifs) et impose à la collectivité un partage des rôles et des responsabilités entre l’affectataire cultuel et la Ville propriétaire.

En tant que maître d’ouvrage public, le DECH établit une programmation des études et travaux poursuivant deux objectifs. Il assure d’une part la compatibilité de l’état sanitaire des édifices avec leur fonction cultuelle. La notion d’établissement recevant du public (ERP) préside alors à l’action du service qui doit faire face à d’importants besoins en matière de sécurisation, de préservation du clos et couvert, de mise en conformité d’installations techniques, de sauvegarde des :ref:`décors <décor>` et des œuvres. D’autre part, il valorise ce patrimoine, véritable collection d’œuvres architecturales. Sont alors prioritaires des :ref:`valeurs <valeur>` symboliques, historiques, archéologiques…

En tant que représentant de la collectivité propriétaire, le DECH constitue et nourrit les dossiers documentaires propres à chaque édifice. Il instruit, du point de vue de l’intégrité patrimoniale de l’édifice, les projets d’aménagement portés par les affectataires, notamment les :ref:`aménagements liturgiques <ménagement liturgique>` en concertation avec la commission diocésaine d’:ref:`art sacré <Art sacré/Art religieux>` et établit les conventions nécessaires aux relations entre la Ville, les affectataires et les tiers.

Le DECH programme et conduit toute forme de travaux, depuis l’entretien (réfection de toiture, remplacement de pierres, de pièces d’orgues…) jusqu’aux grandes opérations (restaurations de toitures ou de façades, de parements intérieurs, d’orgues, rénovation d’installations techniques…). Il dispose pour cela de deux types de budget : budget de fonctionnement (pour les réparations et remplacements) et budget d’investissement, qui comporte des provisions thématiques (sécurisations urgentes, mises en conformité des installations techniques) et des budgets localisés visant un édifice. Un potentiel important d’opérations est présenté en début de mandature à la municipalité. Après validation, ce programme devient le plan d’investissement municipal en faveur du patrimoine cultuel pour la durée de la mandature.

Pour l’ensemble de ces missions, le DECH compte un effectif d’une vingtaine d’agents et rassemble des compétences techniques, historiques, juridiques, administratives et budgétaires. Une mission est consacrée à la gestion du patrimoine organistique et campanaire et à la conduite des opérations le concernant.

Dans la même sous-direction, la relation de travail est étroite entre le DECH et la Conservation des œuvres d’art religieuses et civiles (COARC), le pôle Archéologie du département d’Histoire de l’architecture et de l’archéologie de Paris (DHAAP) et la cellule Mécénat, chacun apportant son expertise aux opérations conduites par le DECH. Au sein des services municipaux, il bénéficie de l’expertise de la direction des constructions publiques et de l’architecture concernant des thèmes transversaux (mise en conformité des ascenseurs, l’éradication de l’amiante ou la mise en :ref:`accessibilité`). Les autorités religieuses et la Conservation régionale des monuments historiques (CRMH) pour les édifices protégés sont ses interlocuteurs extérieurs. De façon générale, le DECH étend à la totalité des 96 édifices des méthodes et doctrines propres au patrimoine protégé.

Au cours des vingt dernières années, les missions du DECH ont été transformées par la décentralisation auprès des :ref:`collectivités territoriales` de la maîtrise d’ouvrage des opérations sur les monuments historiques, par l’augmentation du nombre des édifices protégés au sein du patrimoine cultuel de Paris, la multiplication des projets d’aménagements initiés par les affectataires et l’introduction du mécénat participant au :ref:`financement` des opérations.

Dans une dizaine d’années, la ville de Paris devra statuer, en concertation avec le diocèse, sur le devenir d’églises construites à partir des années 1930 (comme Saint-Antoine de Padoue, 15 ardt), régies par des baux originaux : la ville, propriétaire du terrain, est devenue propriétaire des constructions à mesure de leur élévation, mais n’a pas, durant 99 ans, à assumer de frais d’entretien ni de grosses réparations. Quelle que soit l’évolution de l’ensemble patrimonial constitué par les édifices du culte et tant que perdurera le statut particulier issu de la loi de 1905, la ville de Paris, par l’action du DECH, aura la mission de les préserver et de les transmettre dans le meilleur état de conservation.

**Laurence Fouqueray**





`Télécharger la notice (PDF) <https://api.nakala.fr/data/10.34847/nkl.88ddxhtv.v1/c0b0ef25aa3758ccc3ba6c992cce55b01b45881b?bearer=16ef48e770778355696645e46b26feb3779176d25c0fd282df89cec0047d2075&amp;content-disposition=attachment>`_





