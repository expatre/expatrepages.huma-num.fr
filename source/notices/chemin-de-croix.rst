:orphan:
.. index::
   single: chemin de croix
.. _chemin de croix:


===============
chemin de croix
===============


→ :ref:`iconographie` ; :ref:`procession` ; :ref:`tableau` ; :ref:`textes pontificaux`

Approches historique et religieuse
====================================

La dévotion au chemin de la croix est dans la continuité de la visite des lieux saints, en particulier de la dévotion processionnelle qui consiste à suivre le chemin parcouru par Jésus dans les rues de Jérusalem depuis le Prétoire de Pilate jusqu’au Golgotha.

La grande difficulté de se rendre en Palestine au XIV\ :sup:`e` siècle, après l’échec des dernières croisades, suscite l’apparition d’éléments de substitution : tableaux, édicules et mise en scène (sacri monti) permettant de faire le :ref:`pèlerinage` par la pensée. Ainsi, la pratique de la via crucis ou chemin de la croix permet au :ref:`fidèle <fidèles>` de revivre, dans la prière et la méditation, la montée du Christ vers le Golgotha. Le bienheureux Henri Suso (†1366), dominicain allemand, développe la pratique d’un chemin de la croix spirituel sans le secours de l’:ref:`iconographie`. En revanche, les franciscains, dont l’apostolat à Jérusalem s’appuie sur le parcours historique de la Passion, développent en Europe le chemin de croix visuel et inventent les scènes représentatives des moments les plus significatifs de la montée au Calvaire. Ils ne sont pas les seuls, le bienheureux dominicain Alvare de Cordoue (v. 1350-1430) rapporte cette pratique dans son couvent et l’encourage. La clarisse de Messine Eustochium († 1498) fait ériger de petits monuments dans son monastère relatant les principales scènes de la vie du Christ dont la *via dolorosa* (Berthod, Hardouin-Fugier, p. 89).

Le bref du pape Innocent XII (du 24 décembre 1696, Ad ea per quae) permet aux Frères mineurs d’installer des stations dans leurs églises conventuelles. Par le bref Exponi nobis du 16 janvier 1731, le pape Clément XII étend la possibilité d’ériger des chemins de croix dans n’importe quel :ref:`sanctuaire` (:ref:`église paroissiale`, abbatiale, oratoire conventuel ou privé) à l’unique condition que l’acte soit effectué par un Frère mineur. Ce décret est confirmé par la bulle de Benoît XIII Inter plurima du 5 mars 1726 et par le bref *Cum tantae* du 30 août 1741 de Benoît XIV. Le 10 mai 1742, ce dernier réglemente l’implantation des chemins de croix et leur aspect : matérialisation des stations par des croix en bois, bénites selon la formule du *Rituel*, elles peuvent être accompagnées de scènes peintes ou sculptées (Berthod, Hardouin-Fugier, p.89 et s.). La dévotion ultramontaine est découverte par le jeune clergé français sous l’influence d’Alphonse de Liguori (1696-1787) qui encourage les dévotions populaires. Sous la monarchie de Juillet, de plus en plus d’érections de chemins de croix dans les églises paroissiales sont demandées (Berthod, Hardouin-Fugier, p. 89). Le franciscain Léonard de Port-Maurice (1676-1751) fixe le nombre de stations à quatorze, du procès de Jésus à sa mise au tombeau. Le point de départ et le terme du parcours restent longtemps variables : du prétoire au saint-sépulcre ou du Cénacle à l’Ascension, allant de huit à dix-huit stations. Les stations sont définitivement fixées à la fin du XVII\ :sup:`e` siècle, indulgenciées et confirmées par les :ref:`textes pontificaux` : le bref d’Innocent XII du 24 décembre 1696, *Ad ea per quae*; la bulle de Benoît XIII, *Inter plurima* du 5 mars 1726 ; les avertissements de Clément XII du 3 avril 1731 et ceux de Benoît XIV du 10 mai 1742. Leur nombre est arrêté à quatorze. Ce sont : 1. Jésus condamné à mort ; 2. Jésus chargé de sa croix ; 3. Jésus tombant sous le poids de la croix ; 4. Jésus rencontrant Marie ; 5. Simon de Cyrène aide au portement de croix ; 6. une femme essuie le visage de Jésus ; 7. Jésus tombe pour la seconde fois ; 8. Jésus console les saintes femmes qui le suivent ; 9. Jésus tombe pour la troisième fois ; 10. Jésus est dépouillé de ses vêtements ; 11. Jésus est cloué sur la croix ; 12. Jésus meurt sur la croix ; 13. Jésus est déposé de la croix ; 14. Jésus est placé au sépulcre.

Pour autant, une quinzième station (« Avec Marie dans l’espérance de la résurrection », bénie le 30 octobre 1979) a été ajoutée au chemin de croix construit en 1958, à l’occasion du centenaire des apparitions de la Vierge Marie à Lourdes. Le chemin de croix de la cathédrale de la Résurrection Saint-Corbinien d’Évry (Essonne), construite dans les années 1990, comprend lui aussi une quinzième station.

Les cardinaux, les évêques résidentiels ou titulaires, les supérieurs de l’Ordre des mineurs (franciscains) ont le droit d’ériger un chemin de croix dans une :ref:`chapelle` ou une église à leur convenance. Lors de l’érection, les quatorze stations doivent être bénites selon les formules du *Rituel*. Par le Motu proprio *Pastorale munus* (30 novembre 1963), Paul VI a accordé aux évêques la faculté d’autoriser les prêtres à le faire, excepté dans les paroisses sur le territoire desquelles se trouve un couvent franciscain.

Du point de vue iconographique, les quatorze :ref:`tableaux <tableau>` de la *via crucis* peuvent être disposés dans une église ou une chapelle sous forme représentative. Dès leur apparition sur les murs des églises, vers 1830, de nombreux artistes proposent des compositions peintes dont certaines demeurent célèbres. Avec l’industrialisation des objets religieux, les maisons de fournitures liturgiques proposent des stations réalisées en séries, en métal, métal émaillé, stuc, bois, etc. Les églises rurales se contentent souvent de chromolithographies encadrées. Apparaissent dans le même temps des *via crucis* à usage personnel, qui permettent au fidèle de méditer sur les stations sans se déplacer dans une église [Fig. 2]. Ce sont des livrets comprenant les quatorze stations en reproduction, souvent commentées, ou tout autre support. Les chemins de croix portatifs, en métal, en étoffes ou autres, se développent au XIX\ :sup:`e` siècle.

La pratique du chemin de croix se fait à l’intérieur de l’église, sous la conduite d’un prêtre, en suivant les stations. Le jour de prédilection est le vendredi à trois heures de l’après-midi, mais il peut avoir lieu également le mercredi et pendant les Quatre-Temps. Le chemin de croix peut se faire à l’extérieur de l’église, à travers les rues du village ou de la ville, en certaines occasions comme le Vendredi saint, tel le chemin de croix dit du Grand voyage, à Romans-sur-Isère (Drôme), conçu au XVI\ :sup:`e` siècle. La :ref:`procession` est souvent animée par une confrérie. Les membres des confréries du chemin de la croix ou des Amants de Jésus et Marie au calvaire portent le costume habituel des pénitents. La confrérie de Rome marche pieds nus et porte un sac de couleur cendrée marqué d’une croix rouge sur la poitrine ; le sac est serré à la taille par une corde ; les femmes sont entièrement vêtues de noir. L’archiconfrérie de la Sanch, fondée à Perpignan en 1416, déploie encore aujourd’hui des objets processionnels particuliers lors du cortège — ouvert par un pénitent porteur d’une :ref:`clochette` — les *misteris* (groupes statuaires).                                                                                              

**Bernard Berthod**

Bibliographie
--------------

* Barbier de Montault Xavier, *Œuvres complètes*, t. 8, Poitiers, Imprimerie Blais, Roy et C, 1893, p. 14-266*.

* Berthod Bernard, Hardouin-Fugier Élisabeth, *Dictionnaire des objets de dévotion dans l’Europe catholique*, Paris, L’Amateur, 2006.

* Hahn Waltraud, *Un objet religieux et sa pratique, le chemin de croix portatif aux XIXe et XXe siècles de France*, Paris, Cerf, 2007.

* Thurston Herbert, *Étude historique sur le chemin de la croix*, Paris, Letouzey et Ané, 1907.

* de Zeldegem Amédée, « Aperçu historique sur la dévotion au chemin de croix », *Collectanea franciscana*, 19 (1949), p. 45-142.

* Ticchi, Jean-Marc, « L’introduction du chemin de croix en France : le succès d’une dévotion romaine ? », dans Frédéric Meyer, Sylvain Milbach (dir.), *Les échanges religieux franco-italiens, 1760-1850*, Chambéry, Presses Universitaires Savoie Mont-Blanc, 2010, p. 117-130.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.c0edq7w1>`_





