:orphan:
.. index::
   single: concordat de 1801
.. _concordat de 1801:


==================
concordat de 1801
==================

→ :ref:`Alsace-Moselle` ; :ref:`Articles organiques` ; :ref:`diocèse` ; :ref:`propriété` ; :ref:`réaffectation`

Présentation générale
=======================

Accédant au pouvoir après les dix années révolutionnaires, Bonaparte rétablit en France une paix religieuse qu’il considère comme indispensable à la paix sociale. Après la victoire de Marengo qui l’avait placé dans une position de force, il négocie avec Pie VII un Concordat, accord international bilatéral entre deux puissances souveraines. Au terme de huit mois d’âpres négociations, la *Convention entre Sa Sainteté Pie VII et le Gouvernement français* est signée le 26 messidor an IX (15 juillet 1801). Parmi les principaux négociateurs avaient participé, pour le pape : le Cardinal Consalvi, Secrétaire d’État, assisté de Mgr. Spina, prélat domestique de Sa Sainteté et du père Caselli, théologien. En face, aux côtés du Premier Consul, avaient travaillé : son frère Joseph Bonaparte, conseiller d’État, assisté de Cretet, également conseiller d’État et de l’abbé Bernier, théologien. Les discussions furent parfois tendues et on put craindre la rupture. En définitive, la France obtint gain de cause sur chacun des points qui semblaient essentiels au Premier Consul. Conformément à ses souhaits, le Concordat accorde de larges prérogatives au gouvernement français. Bonaparte fait de l’Église catholique et des autres religions un service public, une institution publique au service sans doute des citoyens et de leurs convictions, mais également de l’État. Le pape obtient cependant le rétablissement du culte public catholique en France.

Le texte est court, conformément à la doctrine de Napoléon selon laquelle il est souvent bon qu’un accord soit « court et obscur » : 17 articles. La religion catholique est reconnue comme celle « de la grande majorité des citoyens français » (préambule) ; elle n’est pas pour autant religion d’État comme l’aurait souhaité le pape. L’exercice public de son culte est libre sous réserve des « règlements de police que le Gouvernement jugera nécessaire pour la tranquillité publique » (art. 1). Le premier Consul nommera les évêques auxquels Sa Sainteté confèrera l’investiture canonique (art. 4) et il exige la démission de tout l’épiscopat alors en fonction car il tient, avant tout, à pouvoir constituer *son* épiscopat. Pour procéder aux désignations épiscopales, Bonaparte édicte une procédure qui reprend celle en vigueur sous l’Ancien Régime, mais dans un tout autre contexte, acceptant la liberté religieuse et donc le pluralisme des croyances et dans un État qui n’est plus aux mains d’un Roi Très Chrétien. Les évêques et le clergé du second ordre prêteront serment d’« obéissance et fidélité » au gouvernement (art. 6). Le Concordat ne fut publié qu’accompagné des :ref:`Articles organiques`, par la loi 18 germinal an X (2 avril 1802). Ces articles concernaient d’une part le culte catholique et d’autre part chacune des deux grandes confessions protestantes (luthérienne et réformée). Des dispositions comparables furent promulguées en 1808 pour l’organisation du culte juif.

Le Concordat de 1801 constitue la pierre angulaire du régime des cultes reconnus qui demeura en vigueur en France jusqu’en 1905. Pourtant, la construction de ce régime s’est faite progressivement, grâce avant tout à la jurisprudence du Conseil d’État. Au cours du siècle, le Concordat de 1801 fut appliqué dans des contextes politiques variés, faisant alterner les périodes d’entente et celles d’anticléricalisme. Le Concordat napoléonien fut abrogé unilatéralement par la France lors de la promulgation de la :ref:`loi du 9 décembre 1905` mettant fin au régime des cultes reconnus et instaurant la Séparation des Églises et de l’État. En vain Pie X condamna-t-il la Séparation par l’encyclique *Vehementer Nos* du 11 février 1906. Toutefois, le Concordat reste en vigueur dans les trois départements de l’Est de la France (Haut-Rhin, Bas-Rhin, Moselle). Au lendemain du traité de Francfort (10 mai 1871), il fut tacitement reconduit par le gouvernement allemand qui y apporta quelques légères modifications. Après la Première guerre mondiale, le régime juridique des cultes et le Concordat furent expressément maintenus au titre du doit local alsacien mosellan (loi du 1juin 1924 et avis du Conseil d’État du 24 janvier 1925). Il est encore aujourd’hui l’une des bases essentielles du droit local alsacien mosellan. Le clergé catholique, et plus largement les ministres des cultes reconnus, sont salariés de l’État.

Aspects patrimoniaux
====================

Les dispositions du Concordat de 1801 consacrées aux biens ecclésiastiques sont limitées – et donc parfois « obscures ». Toutefois, à l’issue de la :ref:`Révolution`, elles déterminent différents aspects du régime de biens qui forment aujourd’hui une part considérable du patrimoine culturel religieux.

Sur cette question patrimoniale comme sur les autres, la marque de l’autoritarisme de Bonaparte se fait sentir. Pie VII doit accepter la confiscation des biens ecclésiastiques effectuée sous la Révolution. Sa Sainteté « déclare que ni elle ni ses successeurs ne troubleront en aucune manière les acquéreurs des biens ecclésiastiques aliénés, et qu’en conséquence la propriété de ces mêmes biens, les droits et revenus y attachés, demeureront incommutables entre leurs mains ou celles de leurs ayant cause » (art. 13). La renonciation de l’Église à son ancien patrimoine est donc définitive. Outre la perte financière considérable, il y a là une sorte de reconnaissance forcée de la part de l’autorité romaine de l’œuvre accomplie par les révolutionnaires. En contrepartie, évêques et :ref:`curés <curé>` recevront un « traitement convenable » du gouvernement (art. 14), disposition qui assurait sans doute les ressources minimales nécessaires, mais qui ne laissait pas au :ref:`clergé` le bénéfice d’une totale indépendance vis-à-vis du gouvernement. Ces traitements existeront mais la disposition n’englobait pas tous les membres du clergé. En pratique, les évêques subirent une nette réduction de leurs ressources par rapport à ce dont ils disposaient avant la révolution ; les curés y gagnèrent plutôt, du moins pour ceux occupant une cure qualifiée de premier ordre. Le Concordat passait sous silence les autres membres du clergé. Bonaparte ne souhaitait pas une renaissance massive des :ref:`congrégations <congrégation>` supprimées lors de la Révolution, de leur patrimoine et de leur force dans la société. Celles-ci renaîtront néanmoins et deviendront puissantes au cours des décennies suivantes. Les :ref:`fidèles` sont autorisés à faire des fondations en faveur des églises (art. 15).

La disposition la plus importante en matière patrimoniale était certainement l’article 12 : « Toutes les églises métropolitaines, :ref:`cathédrales <cathédrale>`, paroissiales et autres non aliénées, nécessaires au culte, seront mises à la disposition des évêques ». Telles étaient les seules prescriptions visant expressément les biens cultuels, dispositions plus que laconiques. Seules étaient visées les églises et non les autres biens, notamment les palais épiscopaux, les presbytères ou les cimetières et les objets mobiliers. Qui allait en détenir la :ref:`propriété` ? Selon quels critères serait évaluée la nécessité pour le culte ? Que recouvrait la mise à la disposition ? Les :ref:`Articles organiques` donneront quelques indications encore peu précises et le Conseil d’État interviendra également. À travers toute la France, un certain nombre d’édifices apparurent à l’administration impériale comme n’étant pas « nécessaires au culte » [catholique] et furent transformées en temples protestants, affectés à la Confession d’Augsbourg ou à l’Église réformée, comme le temple Saint-Ruf à Valence (Drôme) ou le temple du Marais (Paris, 4). En vertu du Concordat, le gouvernement français créa 60 :ref:`diocèses <diocèse>`, dont 10 sièges métropolitains. Napoléon voulait un épiscopat peu nombreux pour deux raisons : afin de le contrôler étroitement mais aussi pour des motifs financiers, afin de limiter les rémunérations dues aux hauts dignitaires ecclésiastiques. La réduction concordataire du nombre de diocèses entraîna celle des édifices pouvant être désignés comme cathédrales (v. Leniaud, p. 15). Sous la Restauration, une loi de 1821 créa 30 nouveaux évêchés, que Louis XVIII s’engageait à doter.

**Brigitte Basdevant-Gaudemet**


Bibliographie 
--------------

*Concordat et recueil des bulles et brefs de N.S.P. le pape Pie VII sur les affaires actuelles de l’Église de France*, par le cardinal Caprara, Paris, 1802.

*Répertoire Dalloz*, 1853, v° Culte.

Ardura Bernard, *Le Concordat entre Pie VII et Bonaparte*, Paris, Cerf, 2001.

Basdevant-Gaudemet Brigitte, *Le jeu concordataire dans la France du XIXe siècle*, Paris, PUF, 1988.

Chantin Jean-Pierre, *Le régime concordataire français. La collaboration des Églises et de l'État (1802-1905)*, Paris, Beauchesne, 2010.

Mgrs Doré, Raffin (dir.), *Le bicentenaire du Concordat*, Strasbourg, Signe, 2002.

Leniaud Jean-Michel, *Les cathédrales au XIXe siècle*, Paris, Économica, 1993.

Minnerath Roland, « Le concordat de Bonaparte et son actualité », *Napoleonica. La Revue*, 2015/2, n 23, p. 4-20.







`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.91b4abj6>`_





