:orphan:
.. index::
   single: crypte
.. _crypte:


===============
crypte
===============

Présentation générale
=====================

La crypte dans l’histoire de l’architecture peut être définie comme une structure voûtée placée principalement sous le chevet, ou au contact du chevet, d’un lieu de culte et destinée à recevoir un corps saint ou les :ref:`reliques <relique/reliquaire>` de martyrs. Le terme *crypte *est issu du grec *kruptos *signifiant « caché ». Cryptes, au pluriel *criptae* dans des sources textuelles, peut désigner plusieurs espaces voûtés appartenant au même édifice. La crypte est présente dans toute l’Europe médiévale et moderne. Son architecture a évolué en fonction des besoins de la :ref:`liturgie` et du statut des édifices. Elle peut revêtir un aspect modeste par ses dimensions ou son :ref:`décor`, ou, au contraire, être une œuvre architecturale quasi équivalente à l’édifice qui la surmonte. Cette situation l’a contrainte dans un grand nombre de cas à être semi-enterrée. Par extension, on a souvent considéré qu’il s’agissait d’espace souterrain et enterré sans que cela soit toujours le cas. De manière plus générale, la crypte est avant tout une construction voûtée. Cette notion d’espace voûté en contexte chrétien trouve son origine chez Grégoire de Tours (538-594), où le terme de crypte apparaît pour qualifier des mausolées funéraires voûtés, c’est-à-dire des lieux protégés. Certains hypogées sont consacrés à des martyrs, comme Martial à Limoges, Privat à Mende ou Bénigne à Dijon. De nombreuses cryptes peuvent aussi répondre aux contraintes du terrain en pente et permettre dans certains cas le soutènement des chevets notamment à l’époque romane. Dans un usage différent du simple caveau ou de la chambre funéraire propre à plusieurs civilisations, la crypte requiert un emplacement particulier sous l’édifice, en principe le chevet, mais cela peut aussi être les bras de transept comme à Saint-Just de Lyon. La crypte sous-entend enfin une accessibilité permanente par des systèmes d’escaliers permettant une utilisation liturgique, notamment lors des :ref:`processions <procession>`.

Dans son lien à l’espace liturgique, la crypte révèle sa fonction propre de lieu de vénération d’un corps saint. Son dispositif original n’est pas toujours lisible derrière les transformations successives, mais renvoie à la mise en scène du corps ou des reliques, selon les époques. L’espace dévolu au recueillement, inclus dans une liturgie processionnelle, devient progressivement un lieu plus ou moins développé en superficie selon le statut de l’édifice et les moyens mis en œuvre. Cela peut aller de la simple salle de 4 m² à une surface équivalente à celle du transept ou de la nef de l’église supérieure. En principe, dans les plus anciennes réalisations, quelle que soit sa dimension, l’:ref:`autel` de célébration du sanctuaire de l’église est installé au-dessus de l’emplacement des reliques de la crypte ; ceci, dans la tradition romaine des autels mis en place directement au-dessus d’un réceptacle appelé « confession », contenant des reliques de martyrs.

La majorité des cryptes médiévales est liée à des abbatiales ou des prieurales, où les religieux ont mission de prières auprès des corps saints et d’accueil des pèlerins, en visites quotidiennes ou en :ref:`pèlerinage`. Dans leur développement, la présence de plusieurs autels permet de multiplier les :ref:`messes <messe>` privées pour le repos de défunts. On peut cependant en trouver dans des églises collégiales, des :ref:`paroissiales <église paroissiale>` et ou encore des :ref:`cathédrales <cathédrale>`. Ces dernières n’ont pas pour vocation d’accueillir spécifiquement des corps saints, mais la vénération d’évêques fondateurs (Reims), d’une statue (Clermont) ou de reliques ayant joué un rôle dans la protection du chef-lieu de diocèse, comme le Voile de la Vierge à Chartres, ont pu engendrer des cryptes. Ainsi, la crypte, :ref:`lieu de mémoire <lieux de mémoire>` et de médiation, doit être replacée dans un contexte historique, local ou régional. Les relations connues sont plus ou moins légendaires du ou des saints vénérés au lieu et éclairent par ailleurs les étapes des premières christianisations ou de la construction a posteriori de cette histoire. La crypte apparaît souvent à tort, avant toute étude archéologique, comme la preuve de la précocité de l’établissement du lieu de culte.

Approche architecturale
========================

Plusieurs auteurs depuis Viollet-le-Duc ont retracé pour la France l’évolution et l’originalité des plans des cryptes, tels R. De Lasteyrie, L. Maître, L. Coutil et F. Deshoulières dans la première moitié du XX\ :sup:`e` siècle. D’autres se sont appliqués à comprendre l’origine de la vénération des reliques. Depuis, les fouilles de nombreux édifices de culte avec crypte, en France comme à l’étranger, ont permis de réévaluer non seulement certaines chronologies, mais plus encore les dispositifs liturgiques. Après un usage restreint, dès le V siècle, dans une simple salle (Saint-Gervais de Genève, *Fig. 1*), plus ou moins vastes, combinées ou non avec d’autres salles (Clos de la Lombarde à Narbonne, Saint-Démétrios de Salonique en Grèce ou Tomis en Roumanie), la demande croissante d’accès aux corps saints a nécessité la création de couloirs destinés à faciliter la circulation. Dès le VII\ :sup:`e` siècle à Saint-Pierre de Rome, le pape Grégoire le Grand réalise une crypte annulaire avec un couloir semi-circulaire étroit permettant aux :ref:`fidèles` d’accéder au *loculus* des reliques sans gêner le célébrant à l’autel.

Ce système est repris dans de nombreuses cryptes carolingiennes installé dans un chevet préexistant. C’est le cas à Rome, à Saint-Marc ou à Sainte-Cécile, mais aussi à Ravenne (Saint-Apollinaire in Classe). On retrouve toujours ce modèle dans des constructions nouvelles autour de 800 à Farfa ou Saint-Vincent de Volturno en Italie ; en Suisse à Coire ou Saint-Maurice d’Agaune ; en Allemagne, à Regensburg, Paderborn et Seligenstadt ; ou encore en France, à Saint-Denis (Seine-Saint-Denis) et Saint-Quentin (Aisne). Cette période de diffusion d’un système romain au-delà des frontières va de pair avec des translations ou enlèvements de corps de martyrs depuis les catacombes romaines. Ainsi, Éginhard (v. 770-840) fait procéder à l’enlèvement des reliques de Marcellin et Pierre pour doter ses fondations de Steinbach et de Seligenstadt (Allemagne). Dans une variante à couloirs coudés, le plan dessiné de Saint-Gall montre clairement le succès de ce type de dispositif (*Fig. 2*).

L’importance accordée au culte des reliques à l’époque carolingienne, au sein d’un renouveau de la liturgie, valorise désormais le chevet, l’un des deux pôles de l’église (avec l’avant-nef). Sa structure architecturale peut être plus complexe avec parfois deux niveaux de cryptes, comme à Saint-Germain d’Auxerre entre 841 et 859, dont le niveau inférieur est conservé, oratoire, confession et rotonde hors-œuvre, avec une partie de son décor peint (*Fig. 3*). Ces chevets hors œuvre ont eu une postérité dans le nord de la France, en Belgique et aux Pays-Bas.

Après l’époque carolingienne, le type crypte-halle à plusieurs vaisseaux, avec piliers ou colonnes et chapiteaux, s’impose un peu partout. Il se répand rapidement peu après l’an Mil en Italie (Agliate, Galliano, Civate, Aoste [*Fig. 4*]), en France (Étampes, Saint-Jean-de-Maurienne, Lons-le-Saunier), en Allemagne (Sainte-Marie-du-Capitole à Cologne), ou en Suisse (Amsoldingen).

Ce type de crypte fait disparaître les cloisonnements et l’étroit système de circulation au profit d’un lieu d’assemblée voûté, unique ou accompagné d’autres salles. Les accès sont souvent doubles et situés de part et d’autre du chœur dans le cas de cryptes à vastes déambulatoires et :ref:`chapelles <chapelle>` rayonnantes dès le XI\ :sup:`e` et au cours du XII\ :sup:`e` siècle. C’est le cas des plus anciennes à Saint-Aignan d’Orléans, Saint-Philibert de Tournus, ou aux cathédrales d’Auxerre, Chartres (*Fig. 5*), Rouen et Clermont. À Spire (Allemagne) comme à Saint-Eutrope de Saintes, la crypte prend l’apparence d’une grande église souterraine.

À partir du début du XII\ :sup:`e` siècle, alors que la plupart des reliques sont présentées sur un autel du chœur supérieur, les cryptes perdent progressivement leur fonction. Leur construction perdure toutefois pour des raisons techniques afin de servir de soubassement dans certaines grandes constructions gothiques (cathédrales de Bourges ou de Metz), tout étant un « lieu de mémoire », incluant certaines parties des cryptes antérieures dans le nouvel édifice. Les exemples des XVII\ :sup:`e` -XVIII\ :sup:`e`  siècles sont rares et il faut attendre les XIX\ :sup:`e` et XX\ :sup:`e` siècles pour voir se développer de nouvelles cryptes qui ont perdu le plus souvent leur fonction originelle pour servir d’églises basses ou de chapelles d’hiver, ou dans certains cas de salle du :ref:`trésor` (Huy, Belgique).

**Christian Sapin**

 
Bibliographie
--------------

* Crook John, *The Architectural Setting of the Cult of Saints in the Early Christian West, c. 300-1200*, Oxford, Clarendon Press, 2000.

* Deshoulieres François, « Les cryptes en France et l’influence du culte des reliques sur l’architecture religieuse », dans *Mélanges en l’hommage de Fr. Martroye*, Paris, C. Klincksieck, 1941, p. 215-237.

* Gillon Pierre et Sapin Christian (dir.), *Cryptes médiévales et culte des saints en Île-de-France et en Picardie*, Lille, Presses universitaire du Septentrion, 2019.

* Magni Marie-Clotilde, « Cryptes du haut Moyen Âge en Italie : problèmes de typologie du IX\ :sup:`e` siècle jusqu’au début du XI\ :sup:`e` siècle », *Cahiers archéologiques*, n 28, 1979, p. 41-85.

* Sapin Christian, *Les cryptes en France*, Paris, Picard, 2014.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.4f2ench4>`_



