:orphan:
.. index::
   single: pèlerinage
.. _pèlerinage:


===============
pèlerinage
===============


→ :ref:`basilique` ; :ref:`fidèles` ; :ref:`procession`; :ref:`relique/reliquaire` ; :ref:`sanctuaire` ; :ref:`tourisme`

Approches historique et religieuse
==================================

La notion de pèlerinage n’est pas proprement chrétienne (Vincent, 2004). Dès la haute Antiquité, des hommes entreprennent un voyage, depuis leur lieu de séjour vers un autre lieu, généralement un :ref`sanctuaire`, pour y implorer la divinité ou le saint personnage qui y est vénéré. Le pèlerinage implique donc un homme croyant, un déplacement et une destination. Le pèlerinage chrétien naît dès les premiers siècles de notre ère ; Jérusalem et les lieux où Jésus a vécu deviennent une destination pour les premiers chrétiens. Après la proclamation de l’Édit de Milan, en 313, et les travaux entrepris par la famille impériale en Terre Sainte, le pèlerinage s’organise depuis l’Europe. Parallèlement, et dès le III\ :sup:`e` siècle, les tombes des apôtres Pierre et Paul, à Rome, sont sanctuarisées et attirent des visiteurs.

Les lieux de pèlerinage se multiplient au cours des siècles. Le pèlerinage par excellence demeure Jérusalem malgré les difficultés du voyage et le départ des :ref:`reliques <relique/reliquaire>` dominicales après la prise de la ville par les musulmans en 638. Constantinople devient alors le lieu où l’on vient vénérer les reliques de la Passion. À Rome, les tombes des apôtres, sur lesquelles l’empereur Constantin a fait édifier deux grandes :ref:`basiliques <basilique>`, sont l’objet d’un pèlerinage que les papes encouragent, en particulier Boniface VIII, en instituant l’année jubilaire en 1300, renouvelée tous les cinquante ans, puis tous les 25 ans ainsi que la visite *ad limina apostolorum* demandée aux évêques après les réformes tridentines. Au cours du Moyen Âge, d’autres lieux en Europe deviennent une destination pour les pèlerins : lieux d’apparition comme celle de saint Michel au Galgano puis en Normandie, au mont Tombe [Fig. 1] ; tombeaux de saints thaumaturges comme celui de Martin, à Tours, de l’apôtre Jacques à Compostelle, de sainte Foy à Conques, de Thomas Becket à Canterbury et bien d’autres. Certains pèlerinages demeurent locaux, d’autres prennent une dimension européenne. Les XIX\ :sup:`e` et XX\ :sup:`e` siècles sont marqués par de nombreuses apparitions mariales en Europe donnant naissance à des pèlerinages internationaux comme Lourdes et Fatima.

La démarche du pèlerin est une démarche de foi avec de nombreuses composantes. La notion de pèlerinage est associée au désir de voir un lieu particulier où a vécu le saint vénéré ; les Lieux saints, la terre foulée par le Christ en est l’exemple le plus évident. Le pèlerin est mu par une prière de demande soit de guérison personnelle ou d’un proche, soit de conversion ; la démarche est souvent entreprise à la suite d’un vœu et donne naissance, si le vœu se réalise, à un :ref:`ex-voto` déposé sur place. Le désir du pèlerin est aussi de se purifier en implorant la rémission des péchés : au Moyen Âge, de nombreuses peines criminelles sont commuées en pèlerinage.

Avec l’apparition du chemin de fer, au milieu du XIX\ :sup:`e` siècle, la multiplication des lieux d’apparition mariale et le retour de la dévotion populaire, les pèlerinages organisés se développent [Fig. 2]. Les premiers sont établis par le père Emmanuel d’Alzon et les prêtres de la :ref:`congrégation` qu’il a créée, les Augustins de l’Assomption [Fig. 3], sous le nom de Notre-Dame du salut, incluant bien l’idée du pèlerinage comme moyen de salvation. Les assomptionnistes emmènent les pèlerins français à Lourdes, à Rome, sur la tombe des apôtres et auprès du « pape captif » et aussi en Terre sainte dans un esprit fortement pénitentiel. Les ordres religieux organisent de leur côté des pèlerinages vers la tombe de leurs propres saints, comme les Carmes auprès du corps de Thérèse de Jésus à Alba de Tormes et de Thérèse de l’Enfant-Jésus et de la Sainte-Face, à Lisieux. À leur tour, les :ref:`diocèses <diocèse>` organisent des pèlerinages proches ou lointains. Les dernières décennies du XX\ :sup:`e` siècle voient apparaître le :ref:`tourisme` religieux associant la découverte d’un lieu ou d’une région à une démarche de foi. Les voyages sont organisés par des congrégations religieuses ou des associations à but spirituel.

Les pèlerinages connaissent une :ref:`patrimonialisation` certaine. Outre la protection des :ref:`objets mobiliers <objet mobilier>` [Fig. 4] et des lieux de pèlerinage, comme les :ref:`chapelles <chapelle>` [Fig. 5] ou les sanctuaires [Fig. 6], une dimension culturelle et touristique est susceptible de s’ajouter à la démarche de foi [Fig. 7]. C’est ce qu’illustrent aussi certains des :ref:`Itinéraires culturels européens <itinéraire culturel européen>` qui empruntent les mêmes chemins que les pèlerins ou encore la création, à la fin des années 2010, d’une *Via Columbani*, visant à recomposer la route empruntée par saint Colomban, moine irlandais du VII\ :sup:`e` siècle, à travers l’Europe.

**Bernard Berthod**

Bibliographie
--------------

* Bordigoni, Marc, « Le ‘pèlerinage des Gitans’ », entre foi, tradition et tourisme », *Ethnologie française*, vol. 32, n° 3, 2002, p. 489-501.

* Branthomme Henri, Chelini Jean, *Les chemins de Dieu, Histoire des pèlerinages chrétiens des origines à nos jours*, Paris, Hachette, 1982.

* Dupront Alphonse, « Tourisme et pèlerinage, réflexions sur la psychologie collective », *Communications*, n° 10, 1967, Paris, p. 97-121.

* Vincent Catherine, *Identités pèlerines*, PUR, Université Rouen-Le Havre, 2004.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.f0af1a04>`_





