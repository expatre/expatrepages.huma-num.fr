:orphan:
.. index::
   single: procession
.. _procession:


===============
procession
===============


→ :ref:`croix` ; :ref:`ostension` ; :ref:`objet processionnel` ; :ref:`patrimoine immatériel religieux` ; :ref:`relique/reliquaire`

La procession, selon son étymologie latine *procedere* est l’action d’aller en avant, en une marche à caractère rituel et communautaire que l’on retrouve dans la plupart des religions primitives, chaldéenne et égyptienne, puis les religions juive et chrétienne. Le groupe qui la compose va d’un point de départ à un lieu d’arrivée et s’accompagne de chants et de prières. Elle est soit liturgique, soit votive et/ou populaire. Dans le catholicisme, la procession est une occasion pour les :ref:`fidèles` d’une ville de faire corps, rassemblant selon un ordre bien précis tous les groupes particuliers de fidèles, corporations, confréries et ordres religieux de la ville.

Approche historique
====================

Les processions liturgiques chrétiennes apparaissent après la paix de l’Église (310) lorsque les manifestations publiques du culte sont autorisées. À Jérusalem, les chrétiens résidents ou en pèlerinage commémorent de façon rituelle les entrées de Jésus dans la ville sainte : la présentation au temple, fêtée le 2 février, et celle de la montée à Jérusalem, dite des Rameaux, qui toutes deux offrent un moyen de revivre les mystères de l’Évangile et de les méditer.

À Rome, au VII\ :sup:`e` siècle, la célébration eucharistique des jours de fêtes, de l’Avent et du Carême, est précédée d’une procession allant d’une église de rassemblement (*collecta*) à l’église stationale où le pontife romain célèbre la :ref:`liturgie`. Ce système est imité à Ravenne et Milan puis par de grandes cités gauloises ou germaniques. Cette pratique a été abandonnée avec l’installation de la papauté en Avignon. Le pape Sixte V essaie de les restaurer, sans succès ; elles demeurent inscrites au Missel romain mais le pape ne se déplace plus. Jean XXIII, en 1959, a repris cet usage pour certaines stations et Paul VI et ses successeurs l’ont continué pour l’entrée en Carême, le mercredi des Cendres, à Sainte-Sabine. Au XII\ :sup:`e` siècle, toujours à Rome, apparait la procession du cierge pascal la nuit de Pâques, procession restaurée avec la vigile pascale par Pie XII en 1951. En Orient, un grand nombre de processions caractérisent les célébrations liturgiques à Constantinople, à Antioche et en Égypte. Le typicon de Constantinople en indique soixante-huit qui rythment l’année liturgique. La procession funèbre qui accompagne le défunt à l’église puis au cimetière s’installe à l’époque mérovingienne ; elle correspond à la procession des anges et des saints qui viennent accueillir le défunt pour le conduire à la Jérusalem céleste.

Outre les processions liturgiques, des processions votives sont organisées sous la pression de la dévotion des fidèles et encadrées par l’Église. Les premières veulent honorer la Vierge Marie ; elles se multiplient dès l’époque carolingienne un peu partout en Europe, marquant les grandes fêtes mariales liturgiques. À Rome, Serge 1er (687-701) initie une procession allant de l’église Sainte-Marie Antique, au forum, à Sainte-Marie-Majeure, pour les fêtes de la Présentation au temple (2 février) de l’Annonciation (25 mars), de la Dormition (15 août) et de la Nativité (8 septembre). Au XVI\ :sup:`e` siècle, après le :ref:`concile` de Trente, les pèlerinages locaux se développent dans chaque diocèse. En France, après la consécration du royaume à la Vierge par Louis XIII, le 15 août est particulièrement marqué.

Quant aux processions eucharistiques, elles se concrétisent après 1264 autour de la célébration du *Corpus Domini*, institué par Urbain IV. Le développement du culte eucharistique à la fin du Moyen-âge puis, surtout, après le concile de Trente, va rendre cette procession extrêmement importante. Elle est encore vivace aujourd’hui partout dans le monde et à Rome où Jean Paul II l’a rétablie entre les basiliques Saint-Jean de Latran et Saint-Marie-Majeure.

La passion du Seigneur génère les processions de la Semaine sainte, au cours desquelles les fidèles revivent les grands moments du Vendredi saint mis en scène par des acteurs ou des statues imposantes. À Jérusalem, les pèlerins déambulent dans les rues de la ville tout au long de la *via crucis* qui est exportée en Europe par les franciscains et répandue par la construction de *sacri monti*, de :ref:`calvaires <calvaire>` qui aboutissent au XIX\ :sup:`e` siècle aux processions du :ref:`chemin de croix`.

Dans chaque :ref:`paroisse` enfin, la fête patronale donne lieu à une procession qui fait le tour de la paroisse ou de la ville en transportant un ou plusieurs :ref:`reliquaires <reliquaire>` du saint patron tandis que dans les campagnes, une procession a lieu avant les récoltes pour les favoriser : ce sont les Rogations [Fig. 1], qui se déroulent trois jours de suite avant l’Ascension.

**Bernard Berthod**

Approche religieuse 
===================

Les règles relatives aux processions sont décrites dans le *Rituel romain* – « livre liturgique qui rassemble les rubriques et formules d’administration des sacrements […] et des rites connexes » (Molin, Aussedat-Minvielle, p. 9) – qui indique un ordre de marche mettant en scène le clergé et les fidèles organisés en confréries et corporations. Les membres du clergé marchent deux à deux derrière la croix et le thuriféraire. Certains groupes de laïcs précédent le clergé : les sociétés de musique, les associations pieuses avec leur bannière, les confréries en costume avec leur bannière, les tiers-ordres en costume. Ensuite les membres du clergé régulier, en corps, précédés de leur :ref:`croix` processionnelle, le clergé séculier, le :ref:`chapitre` de la :ref:`cathédrale`, un thuriféraire, le porte-croix entouré de deux acolytes, les prélats, les cérémoniaires, l’officiant ; si l’officiant est évêque, il est suivi par son caudataire, les porte-:ref:`insignes <signes/insignes religieux>`, les membres de la famille épiscopale puis les fidèles. La faveur des processions a amené la Congrégation pour le culte divin et la discipline des sacrements à réglementer les coutumes liées au déroulement de l’année liturgique et à la piété mariale, le recours aux anges, saints et saints locaux, et à recommander d’encadrer le comportement des fidèles vis-à-vis des  :ref:`objets processionnels <objet processionnel>` (*Directoire sur la piété populaire et la liturgie : principes et orientations*), qu’il s’agisse des « reliques des saints » (236-237) ou des « processions » (245-247).

**Bernard Berthod**

Approche juridique
===================

Les processions sont encore aujourd’hui régies par la :ref:`loi du 9 décembre 1905`, même si leur appréhension en droit a quelque peu évolué depuis, reflétant un certain changement de regard sur cette pratique religieuse. Selon l’article 27 de la loi de Séparation, « les cérémonies, processions et autres manifestations extérieures d’un culte, sont réglées en conformité de l’article L. 2212-2 du Code général des collectivités territoriales », qui renferme les dispositions classiques relatives à l’ordre public assuré par la police municipale. Le texte de l’article 27 soumis à la discussion parlementaire était initialement formulé dans le sens d’une interdiction « sur la voie publique » des « cérémonies, processions et autres manifestations extérieures d’un culte », les cérémonies funèbres devant être réglées « dans toutes les communes par arrêté municipal dans les conditions de la loi du 15 novembre 1887 ». Cette interdiction avait été présentée par le rapporteur de la loi « comme la consécration du principe de liberté et de neutralité », un moyen de « séparation entre le monde religieux et le monde laïque, comme entre les divers groupements religieux » (Briand, p. 292), qui présidait également à l’interdiction des :ref:`signes religieux <signes/insignes religieux>` contenue dans l’article 28 de la loi. Ces dispositions répondaient aussi implicitement au souhait d’éviter de laisser entre les mains des maires le sort – de l’interdiction – des processions et d’atteindre une application homogène de la loi sur le territoire. Le *statu quo* l’emporta, notamment au nom d’une certaine cohérence avec la proclamation par l’article 1 de la loi de la liberté de culte, mais également d’une continuité des traditions en la matière.

Les dispositions finalement adoptées mettent sur un apparent pied d’égalité cérémonies, processions et autres manifestations extérieures d’un culte, mais l’abondante jurisprudence dans la décennie suivant la séparation établira une certaine différenciation entre d’un côté les processions et de l’autre les convois funèbres auxquels est le plus souvent associé le port du viatique. Si les tribunaux manifestent une souplesse à l’égard de l’interdiction des premières, jugeant simplement que le maire n’a fait qu’user des pouvoirs de police qui lui sont conférés dans l’intérêt de l’ordre public (voir par exemple CE, 10 mars 1911, *Abbé* *Delmejà : Lebon* p. 286), ils annulent les arrêtés municipaux formulés en des termes trop généraux ayant pour conséquence de prohiber « d’autres cérémonies telles que les convois funèbres et le port du viatique » (CE, 30 janv. 1914, *Abbé Boillot : Lebon* p. 108 ; voir aussi l’arrêt de principe : CE, 19 févr. 1909, *Abbé Olivier : Lebon* p. 186) et « qu’elles étaient pratiquées traditionnellement dans la commune. » (CE, 4 avril 1914, *Abbé Cibot et autres : Lebon* p. 483).

En dépit d’un certain renouveau dans la période contemporaine, les processions sont désormais loin des tensions du début du XXe siècle, dont on ne trouve désormais qu’un lointain écho devant les tribunaux, qu’il s’agisse de leur :ref:`financement` par des :ref:`collectivités locales <collectivités territoriales>` (`CE, 15 févr. 2013, n°347049 <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000027111114/>`_, *Association grande confrérie de Saint-Martial*, à propos :ref:`ostensions <ostension>` limousines) ou de leur organisation : le déroulement d’une procession sur l’île de Houat pour la fête des Rameaux n’était pas « de nature à créer une situation particulièrement dangereuse pour l’ordre public », le refus du maire de l’interdire étant en conséquence justifié (`CAA Nantes, 8 juin 2018, n° 17NT02695 <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000037048470/>`_). Dans cette affaire, les juges précisent d’ailleurs que « l’article L. 211-1 du code de la sécurité intérieure, qui se borne à prévoir une obligation de déclaration préalable des manifestations sur la voie publique, sauf lorsqu’elles sont conformes aux usages locaux, est inopérant pour contester la décision du maire de l’île d’Houat refusant d’interdire la procession litigieuse ».

Outre l’article 27 de la loi du 9 décembre 1905, qui demeure l’essentiel du droit applicable, méritent d’être mentionnées les dispositions législatives et réglementaires liées à l’interdiction de la dissimulation du visage dans l’espace public, qui renvoient aux objets processionnels que sont les couvre-chefs de certaines confréries arborés pendant les processions de la Semaine sainte. En effet, « l’interdiction ne s’applique pas ‘si elle s’inscrit dans le cadre de pratiques sportives, de fêtes ou de manifestations artistiques ou traditionnelles’. Ainsi les processions religieuses, dès lors qu’elles présentent un caractère traditionnel, entrent dans le champ des exceptions à l’interdiction posée par l’article 1. » (Circulaire du 2 mars 2011 relative à la mise en œuvre de la loi n° 2010-1192 du 11 octobre 2010 interdisant la dissimulation du visage dans l’espace public).

**Anne Fornerod**

 

Bibliographie
--------------

* Andrieu Michel, « Aux origines du culte du Saint-Sacrement, reliquaires et monstrance », *Analecta Bollandiana*, Bruxelles, 1950, p. 397-418.

* Berthod Bernard, *Rites funéraires catholiques*, Dilaceratio Corporis, Fage, Lyon, 2019.

* Briand Aristide, *Rapport fait le 4 mars 1905 au nom de la commission relative à la séparation des Églises et de l’État et de la dénonciation du Concordat chargée d’examiner le projet de loi et les diverses propositions de loi*, Chambre des députés, 4 mars 1905.

* *La Maison Dieu*, numéro spécial, n° 43 (1955).

* Martimort Aimé-Georges, *L’Église en prière*, Paris, 1984, p. 259-269.

* Molin Jean-Baptiste, Aussedat-Minvielle Annik, *Répertoire des rituels et processionnaux imprimés conservés en France*, Aubervilliers : Institut de Recherche et d'Histoire des Textes (IRHT), Éditions du CNRS, 1984 (`https://www.persee.fr/doc/dirht_0073-8212_1984_cat_32_1 <https://www.persee.fr/doc/dirht_0073-8212_1984_cat_32_1>`_).

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.e5fap428>`_





