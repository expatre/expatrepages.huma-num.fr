:orphan:
.. index::
   single: affectataire cultuel
.. _affectataire cultuel:


======================
affectataire cultuel
======================

→ :ref:`accord préalable de l'affectataire cultuel` ; :ref:`affectation cultuelle` ; :ref:`curé` ; :ref:`gardiennage`

Approche juridique
====================

Figure majeure du patrimoine religieux, l’affectataire cultuel connaît un statut juridique traversé par un paradoxe, né précisément de l’évolution profonde de l’usage des édifices religieux*. Si la personne – physique ou morale – que désigne ce statut est bien connue et ses prérogatives définies depuis longtemps, l’ouverture des édifices cultuels à des usages autres que religieux renvoie à un certain nombre d’interrogations.

Qui est l’affectataire cultuel ? La :ref:`loi de séparation du 9 décembre 1905 <loi du 9 décembre 1905>` désigne les :ref:`associations cultuelles <affectation cultuelle>` dont elle prévoit la création (articles 4 et 19) et auxquelles sont mis à disposition « les édifices servant à l’exercice public du culte, ainsi que les objets mobiliers les garnissant » (art. 13). Apparaît ici le lien étroit, logique et déterminant avec l’:ref:`affectation cultuelle`, qui résulte également, et surtout, de la loi du 2 janvier 1907 pour les édifices du culte catholique, qui, à la différence des cultes protestant et israélite, n’a pas constitué d’associations cultuelles en 1905. Ces édifices sont alors laissés « à la disposition des :ref:`fidèles` et des ministres du culte pour la pratique de leur religion » (art. 5). Le droit prend en compte l’organisation interne de l’Église catholique dans la mise en œuvre de ces textes en faisant des ministres du culte en communion avec leur hiérarchie* les affectataires des édifices antérieurs à 1905. La jouissance des lieux suppose pour l’affectataire qu’il respecte les règles d’organisation générale du culte*, « lesquelles comprennent, en ce qui concerne la religion catholique, la soumission à la hiérarchie* ecclésiastique » (CE, 23 janv. 1920, Abbé Barraud et autres : Lebon p. 75). Les emprunts à l’organisation religieuse se manifestent dans les variations terminologiques pour désigner l’affectataire – le :ref:`curé`, l’abbé, l’ecclésiastique… Il est en outre assez fréquent de trouver dans la jurisprudence le terme de desservant, venant du droit des cultes napoléonien et qui désigne, dans une logique similaire, « le prêtre affecté par l’évêque au service de la circonscription nommée succursale par la loi du 18 germinal an X » (Naz, tome 4). L’instauration, en 1924, des associations diocésaines, comme équivalent des associations cultuelles pour l’Église catholique, n’a pas fondamentalement changé la situation. Certes, c’est l’évêque, président de droit de l’association diocésaine, qui est visé par le décret n° 70-220 du 17 mars 1970 portant déconcentration en matière de :ref:`désaffectation <désaffectation cultuelle>` des édifices cultuels lorsqu’il prévoit l’accord préalable de « la personne physique ou morale ayant qualité pour représenter le culte affectataire » (art. 1er). Pour autant, la figure emblématique du curé de la :ref:`paroisse` continue d’incarner l’affectataire cultuel. Encore en 2011, la circulaire du ministère de l’Intérieur consacrée aux édifices du culte (NOR : IOCD11/21246C) indique que « l’affectataire est le curé desservant l’église (ou les églises) de la paroisse, nommé par l’évêque du diocèse territorialement compétent et chargé de régler l’usage des lieux de manière à assurer aux fidèles la pratique de leur religion. » (p. 7).

L’affectataire dispose d’un ensemble de prérogatives « pour les cérémonies cultuelles et pour assurer la police du lieu consacré au culte dans l’exercice des pouvoirs qui lui appartiennent. » (CE, 3 mai 1918, `Abbé Piat <https://nakala.fr/10.34847/nkl.bef4595x>`_ : *Lebon* p. 409). Ainsi en va-t-il des horaires d’ouverture et de fermeture (CE, 24 févr. 1912, *Abbé Sarralongue : Lebon* p. 250) et de la disposition de la clé de l’édifice (CE, 11 avril 1913, *Abbé Sommé, Lebon* p. 392 ; CE, 20 juin 1913, *Abbé Arnaud : Lebon* p. 717). La mise en œuvre de l’affectation cultuelle lui donne aussi compétence pour décider de l’aménagement intérieur des lieux, qu’il s’agisse des sièges (CE, 15 janv. 1937, *Sieur de Bonnafos et autres : Lebon* p. 48) et du mobilier de manière générale ou encore du décor intérieur : la réalisation d’une fresque dans la chapelle de `Notre-Dame-du-Château <https://nakala.fr/10.34847/nkl.675buf8o>`_ [Fig. 1], commandée par la commune de Saint-Étienne-du-Grès (Bouches-du-Rhône) « n’avait pas le caractère de travaux nécessaires à la conservation et à l’entretien de l’édifice », mais « relevait de la seule compétence du ministre du culte en charge de la garde et de la police de ladite chapelle » (CAA Marseille, 22 nov. 2011, n° 10MA00428, `Commune de Saint-Étienne-du-Grès <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000025115768>`_).

Les termes de « garde » et surtout de « police » reviennent très régulièrement en jurisprudence pour qualifier ces attributions de l’affectataire cultuel (CE, 3 mai 1918, *Abbé Piat : Lebon* p. 409 ; CE, ord., 25 août 2005, `Commune de Massat <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000024364411/>`_ : *Lebon* p. 386). Il est possible d’y voir un emprunt à la terminologie juridique relative à la police administrative et particulièrement aux polices spéciales qui se caractérisent par un pouvoir de réglementation des autorités compétentes, déterminé essentiellement par leur objet, un domaine d’activité spécifique. Si ses diverses compétences traduisent le fait que l’affectataire est, à la différence des fidèles, bien plus qu’un usager du domaine public cultuel, elles sont finalisées, l’affectataire ayant « la garde et la police de l’église en vue d’assurer aux fidèles l’exercice de leur religion » (CE, 26 déc. 1930, *Abbé Tisseire : Lebon* p. 1114). Il en découle que l’affectataire « n’assume aucune obligation de caractère matériel, telle qu’une obligation de sécurité qui tendrait à l’assimiler à l’exploitant d’un lieu ouvert au public ou à une collectivité publique administrant un bien du domaine public » (Cass. 2 civ., 19 juil. 1966, *SNCF et dame Vautier c/ Chanoine Rebuffat*, n 63-13337). En effet, le pouvoir de gestion d’un bien du domaine public « implique notamment la responsabilité de la conservation du bien, traditionnellement à la charge du propriétaire » (Fornerod, p. 259).

Le :ref:`gardiennage` de l’édifice, le plus souvent confié au curé de la paroisse depuis la loi du 9 décembre 1905 de séparation des Églises et de l’État, sans être totalement éloigné des fonctions de l’affectataire, relève d’une autre logique, dans la mesure où il s’agit de veiller à la conservation matérielle des biens. Par ailleurs, sans que ces prérogatives reconnues de longue date ne soient remises en question, le rôle de l’affectataire cultuel a quelque peu évolué sous l’influence de la patrimonialisation culturelle des édifices et de leur mobilier dont il règle l’usage. En effet, à leur destination religieuse consacrée par l’affectation cultuelle, peuvent s’ajouter des utilisations, le plus souvent culturelles, qui requièrent alors l’accord préalable de l’affectataire.

**Anne Fornerod**

Bibliographie
--------------

* Dieu Frédéric, « Les prérogatives de l’affectataire cultuel : étendue et limites », *Revue du droit des religions*, n 2, 2016, p. 139-156.

* Dufau Jean, *Le domaine public*, Paris, le Moniteur, 5 éd., 2001.

* Fornerod Anne, *Le régime juridique du patrimoine culturel religieux*, Paris, L’Harmattan, 2013.

* Kerlévéo Jean, *L’Église catholique en régime français de séparation*, tome 2, *Les prérogatives du curé dans son église*, Paris, Desclée, 1956.

* Naz Robert, *Dictionnaire de droit canonique*, tome 4, v° « Desservant », Paris, Letouzey et Ané, 1949.**

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.bc32is1o>`_



