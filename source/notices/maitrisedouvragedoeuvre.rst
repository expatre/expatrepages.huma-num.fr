:orphan:
.. index::
   single: maîtrise d'ouvrage/d’œuvre
.. _maîtrise d'ouvrage/d’œuvre:


===========================
maîtrise d’ouvrage/d’œuvre
===========================


→ :ref:`affectataire cultuel` ; :ref:`association cultuelle` ; :ref:`classement monument historique` ; :ref:`entretien/réparations` ; :ref:`responsabilité` ; :ref:`travaux`


Les notions de maîtrise d’ouvrage et de maîtrise d’œuvre sont définies par la loi n° 85-704 du 12 juillet 1985 relative à la maîtrise d’ouvrage publique et à ses rapports avec la maîtrise d’œuvre privée. Ces dispositions, modifiées à plusieurs reprises, ont été codifiées au code de la commande publique, notamment aux articles L. 2411-1 et s. Les exigences relatives à la maîtrise d’ouvrage comme à la maîtrise d’œuvre indiquées ici ne valent que pour les :ref:`édifices religieux <édifice religieux>` qui sont propriété d’une personne publique (la commune le plus souvent), soit l’immense majorité du patrimoine architectural religieux.

« Le maître de l’ouvrage est la personne morale […] pour laquelle l’ouvrage est construit. Responsable principal de l’ouvrage, il remplit dans ce rôle une fonction d’intérêt général dont il ne peut se démettre » (art. 2, L. n° 85-704 du 12 juillet 1985). Il ne peut déléguer cette fonction d’intérêt général, sous réserve, d’une part, des dispositions relatives au mandat et au transfert de la maîtrise d’ouvrage, d’autre part, des dispositions relatives aux contrats de partenariat, qui ne concernent pas le patrimoine religieux.

« Il lui appartient, après s’être assuré de la faisabilité et de l’opportunité de l’opération envisagée, d’en déterminer la localisation, d’en définir le programme, d’en arrêter l’enveloppe financière prévisionnelle, d’en assurer le financement, de choisir le processus selon lequel l’ouvrage sera réalisé et de conclure, avec les maîtres d’œuvre et entrepreneurs qu’il choisit, les contrats ayant pour objet les études et l’exécution des travaux.

Lorsqu’une telle procédure n’est pas déjà prévue par d’autres dispositions législatives ou réglementaires, il appartient au maître de l’ouvrage de déterminer, eu égard à la nature de l’ouvrage et aux personnes concernées, les modalités de consultation qui lui paraissent nécessaires.

Le maître de l’ouvrage définit dans le programme les objectifs de l’opération et les besoins qu’elle doit satisfaire ainsi que les contraintes et exigences de qualité sociale, urbanistique, architecturale, fonctionnelle, technique et économique, d’insertion dans le paysage et de protection de l’environnement, relatives à la réalisation et à l’utilisation de l’ouvrage. » (*Ibid*). Ces exigences ne soulèvent guère de problème et ne trouvent même que rarement à s’appliquer, en ce qui concerne le patrimoine religieux, car ce dernier, sauf cas particulier, y répond *a priori*, le patrimoine religieux est, par définition, un patrimoine existant déjà.

En revanche s’appliquent les dispositions relatives au programme et à l’enveloppe financière prévisionnelle, qui doivent être définis avant tout commencement des études de projets, et qui peuvent cependant être précisés par le maître de l’ouvrage avant « tout commencement des études de projet. Lorsque le maître de l’ouvrage décide de réutiliser ou de réhabiliter un ouvrage existant, ce qui est presque toujours le cas pour le patrimoine religieux, l’élaboration du programme et la détermination de l’enveloppe financière prévisionnelle peuvent se poursuivre pendant les études d’avant-projets ».

« Le maître de l’ouvrage peut confier les études nécessaires à l’élaboration du programme et à la détermination de l’enveloppe financière prévisionnelle à une personne publique ou privée ». (*Ibid*)

« La mission de maîtrise d’œuvre que le maître de l’ouvrage peut confier à une personne de droit privé ou à un groupement de personnes de droit privé doit permettre d’apporter une réponse architecturale, technique et économique au programme […]. 

Pour la réalisation d’un ouvrage, la mission de maîtrise d’œuvre est distincte de celle d’entrepreneur. Le maître de l’ouvrage peut confier au maître d’œuvre tout ou partie des éléments de conception et d’assistance suivants : 1° Les études d’esquisse ; 2° les études d’avant-projets ; 3° Les études de projet ; 4° L’assistance apportée au maître de l’ouvrage pour la passation du contrat de travaux ; 5° La direction de l’exécution du contrat de travaux ; 6° L’ordonnancement, le pilotage et la coordination du chantier ; 7° L’assistance apportée au maître de l’ouvrage lors des opérations de réception et pendant la période de garantie de parfait achèvement.

Toutefois, pour les ouvrages de bâtiment, une mission de base fait l’objet d’un contrat unique. Le contenu de cette mission de base, fixé par catégories d’ouvrages, doit permettre : au maître d’œuvre, de réaliser la synthèse architecturale des objectifs et des contraintes du programme, et de s’assurer du respect, lors de l’exécution de l’ouvrage, des études qu’il a effectuées ; au maitre de l’ouvrage, de s’assurer de la qualité de l’ouvrage et du respect du programme et de procéder à la consultation des entrepreneurs, notamment par lots séparés, et à la désignation du titulaire du contrat de travaux.

La mission de maîtrise d’œuvre donne lieu à une rémunération forfaitaire fixée contractuellement. Le montant de cette rémunération tient compte de l’étendue de la mission, de son degré de complexité et du coût prévisionnel des travaux. » (art. 7, L. n° 85-704 du 12 juillet 1985). Selon l’article L. 621-29-2 du code du patrimoine, les services de l’État chargés des monuments historiques peuvent apporter une assistance gratuite au propriétaire ou à l’affectataire domanial d’un immeuble classé ou inscrit (ce qui est le cas d’une majorité d’édifices du culte) qui ne dispose pas, du fait de l’insuffisance de ses ressources ou de la complexité du projet de travaux, des moyens nécessaires à l’exercice de la maîtrise d’ouvrage de l’opération.

Une assistance de l’État en matière de maîtrise d’ouvrage peut également être apportée lorsqu’aucune des deux conditions précitées n’est remplie, dès lors que le propriétaire ou l’affectataire domanial établit la carence de l’offre privée et des autres collectivités publiques. Dans ce cas, la prestation est rémunérée par application d’un barème établi en fonction des coûts réels, fixé par décret en Conseil d’État. Une convention signée avec le propriétaire ou l’affectataire domanial définit les modalités particulières de l’assistance à la maîtrise d’ouvrage assurée par les services de l’État.

Plusieurs cas de figure sont possibles en cas de travaux à effectuer sur un édifice du culte. Lorsque ce dernier appartient à une personne privée – le plus souvent une association, quelle que soit la forme de cette association – c’est le propriétaire qui assure la maîtrise d’ouvrage et choisit le maître d’œuvre, avec la possibilité, signalée ci-dessus, d’une assistance à maîtrise d’ouvrage par l’État si les conditions légales sont réunies.

Le plus souvent, cependant, pour les raisons historiques que l’on connaît, la propriété des édifices du culte va appartenir aux communes. Plusieurs cas de figure peuvent alors se présenter, sachant que la solution est différente selon que l’édifice est classé ou inscrit au titre des monuments historiques ou pas. Dans le cas où l’édifice est classé, la solution la plus fréquente est celle d’une convention conclue entre la commune et l’État, ce dernier assurant la maîtrise d’ouvrage, l’architecte en chef des monuments historiques (ACMH) se voyant confier la maîtrise d’œuvre (V. par exemple  : CAA Versailles, 11 janv. 2018, n° 15VE03753 ; V. `CAA Lyon <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000021965895?dateDecision=&amp;init=true&amp;page=1&amp;query=%22article+1710%22+du+%22Code+civil%22&amp;searchField=ALL&amp;tab_selection=cetat>`_, 4 mars 2010, n° 07LY02915 pour la distinction avec le contrat de louage d’ouvrage).

La commune propriétaire de l’édifice et maître d’ouvrage des travaux peut assurer elle-même si elle le souhaite la maîtrise d’œuvre. Si tel n’est pas le cas le titulaire d’un contrat de maîtrise d’œuvre est rémunéré pour un prix forfaitaire couvrant l’ensemble de ses charges et missions, ainsi que le bénéfice qu’il en escompte et seules une modification de programme ou une modification des prestations décidées par le maître de l’ouvrage peuvent donner lieu à une adaptation et, le cas échéant, une augmentation de sa rémunération (`CAA Lyon <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000026631385>`_, 7 nov. 2012, n° 11LY02969). Une réception sans réserves ne met fin aux rapports contractuels entre le maître d’ouvrage et les constructeurs qu’en ce qui concerne la réalisation de l’ouvrage mais demeure sans effet sur les droits et obligations financiers nés de l’exécution du marché à raison de désordres provoqués par cette exécution mais n’affectant pas l’état des travaux achevés (V. par ex. `CAA Douai <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000032825162>`_, 23 juin 2016, n° 14DA01340, à propos d’un :ref:`incendie` s’étant déclaré, alors que la commune assurait la maîtrise d’œuvre des travaux sur l’église).

Un cas intéressant est celui d’un conseil municipal ayant autorisé son maire, dans le cadre de la réhabilitation et de l’extension de l’église, à conclure avec l’association diocésaine deux conventions, l’une ayant pour objet, pour une durée de dix ans, d’autoriser l’occupation d’une bande appartenant au domaine public afin de permettre la réalisation des travaux et l’autre fixant les responsabilités des deux parties à l’échéance de la première convention en matière de gestion et d’entretien de l’église. Sur recours d’un requérant ayant contesté cette occupation, le juge déclare que les dispositions applicables (art. L. 1311-5 CGCT) ont ouvert aux :ref:`collectivités territoriales` la faculté, dans le respect du principe de neutralité à l’égard des cultes et du principe d’égalité, d’autoriser un organisme qui entend construire un édifice du culte ouvert au public à occuper une dépendance de leur domaine public pour une durée limitée dans le cadre d’une autorisation temporaire et d’assortir cette autorisation de la construction de droits réels au profit de l’occupant. Des droits réels ont pu être consentis à l’association diocésaine dans les limites et pour la durée de la garantie décennale afin de permettre à ladite association diocésaine de mener à bonne fin la maîtrise d’ouvrage des travaux (`CAA Versailles <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000029191362>`_, 19 juin 2014, n° 12VE02835).

**Jean-Marie Pontier**

Bibliographie
--------------

* Ouzoulias Pierre et Ventalon Anne, `Patrimoine religieux en péril : la messe n’est pas dite <https://www.senat.fr/notice-rapport/2021/r21-765-notice.html>`_, rapport d’information au nom de la commission de la culture, de l’éducation et de la communication, Sénat, n° 765, 2022.

* Pontier Jean-Marie, *La protection du patrimoine culturel*, L’Harmattan, 2019.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.cdcbmc8z>`_



