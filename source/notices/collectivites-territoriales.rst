:orphan:
.. index::
   single: collectivités territoriales
.. _collectivités territoriales:


============================
collectivités territoriales
============================


→ :ref:`entretien/réparations` ; :ref:`financement` ; :ref:`paroisse` ; :ref:`propriété` ; :ref:`loi du 9 décembre 1905`

Approches historique et juridique
=================================

Selon l’article 72 premier alinéa de la Constitution : « les collectivités territoriales de la République sont les communes, les départements, les régions, les collectivités territoriales à statut particulier et les collectivités d’outre-mer régies par l’article 74 ». Les collectivités territoriales à statut particulier sont représentées par Paris, la Corse et la métropole de Lyon (qui, par la volonté du législateur, et contrairement aux autres métropoles, est une collectivité territoriale). Dans les collectivités territoriales de l’article 74 (départements et régions d’outre-mer), les lois et règlements peuvent faire l’objet d’adaptations. On ne s’intéressera pas ici aux collectivités d’outre-mer, régies par des règles spécifiques.

Les collectivités territoriales les plus intéressantes, du point de vue du patrimoine religieux, sont évidemment les communes. Les autres catégories de collectivités territoriales sont moins concernées, sous les réserves apportées ci-après. En effet, les départements n’ont pas d’héritage patrimonial comme peuvent l’avoir les communes, du fait de leur création qui ne date « que » de la :ref:`Révolution`, et ils ont été pendant longtemps avant tout un cadre de déconcentration (d’ailleurs limitée) plus que de décentralisation, le préfet étant l’autorité la plus importante. Enfin, les départements ont eu, jusqu’à une date relativement récente, une « vocation » avant tout sociale plus que culturelle. En ce qui concerne les régions, c’est encore plus simple de ce point de vue. D’une part, créées en 1982, elles disposent d’un patrimoine nécessairement plus limité que celui des autres collectivités. D’autre part, la fonction des régions est plus une fonction de coordination qu’une fonction de gestion. Leur rôle est d’abord économique, le patrimoine religieux ne peut qu’occuper une place limitée dans leurs interventions, même si des changements peuvent se produire dans l’avenir.

Ce sont donc les communes qui sont principalement concernées par le patrimoine religieux. Cela s’explique assez facilement par l’histoire. Les communes ont existé avant qu’il n’y ait un État, elles ont une ancienneté qui les fait remonter loin dans le temps. Certes, il ne s’agissait pas de communes telles que nous les connaissons aujourd’hui, elles ne portaient d’ailleurs pas ce nom. Il existait, sous l’Ancien Régime, une assez grande diversité de modes d’organisation de ce que nous appelons, depuis la Révolution, les communes. Ces communautés locales étaient à l’origine des regroupements de familles. Lors de l’installation des ordres religieux dans notre pays, de l’avancée du christianisme dans les siècles qui ont suivi sa naissance, des :ref:`édifices religieux <édifice religieux>` ont été construits partout où il y avait des groupements de population, :ref:`églises <église paroissiale>` dans les villages et les bourgs, abbayes et monastères dans les endroits isolés. Avec la première Renaissance en Occident, au XII\ :sup:`e`  siècle, avec la paix qui s’installe, les voies de communication qui deviennent plus sûres, une amélioration des conditions de vie, des édifices religieux sont construits partout en Europe. Une citation du moine Raoul Glaber, qui écrivit des chroniques autour de l’an mille, est demeurée célèbre : « Trois années ne s’étaient pas écoulées dans le millénaire que, à travers le monde entier, et plus particulièrement en Italie et en Gaule, on commença à reconstruire les églises […]. C’était comme si le monde entier se libérait, rejetant le poids du passé et se revêtait d’un blanc manteau d’églises » (Glaber, p. 163). Et Jean Gimpel a pu écrire : « pendant les trois siècles de son expansion économique du XI au XIII\ :sup:`e` siècle, la France a charrié plus de pierres que l’Égypte en n’importe quelle période de son histoire » (Gimpel, p. 33).

Les communes françaises présentent une particularité à la fois politique, sociologique et juridique par rapport à leurs homologues européennes : leur nombre très élevé (longtemps autour de 36 500, il a baissé pour se situer aujourd’hui à un peu moins de 35 000), devenu un thème ressassé depuis de longues années. D’un chiffre « très » élevé on est passé, depuis plusieurs décennies, dans les appréciations, à un chiffre « trop » élevé, qui serait préjudiciable à la fois à une bonne administration, à une bonne gestion des fonds publics et, en définitive, à la condition de vie des habitants. Ce nombre ne pouvant être réduit drastiquement, pour plusieurs raisons qu’il n’est pas possible de reprendre ici, les pouvoirs publics ont cherché à contourner le problème en créant des structures de coopération de plus en plus intégratrices, les établissements publics de coopération intercommunale (EPCI) [Fig. 1].

En quoi ceci concerne-t-il le patrimoine religieux des communes ? En vertu de la loi n 98-546 du 2 juillet 1998 (art. 94, modifiant les articles 12 et 13 de la :ref:`loi de 1905 <loi du 9 décembre 1905>`), les EPCI peuvent prendre la compétence en matière d’édifices des cultes, à savoir en devenir propriétaire et/ou participer au :ref:`financement` de leur :ref:`entretien <entretien/réparations>` (voir l’exemple de l’`église Saint-Pierre de Firminy Vert <https://www.legifrance.gouv.fr/ceta/id/CETATEXT000024803117?init=true&amp;page=1&amp;query=%C3%A9glise+firminy&amp;searchField=ALL&amp;tab_selection=all>`_, classée monument historique, cédée à la Communauté d’agglomération Saint-Étienne Métropole par une association en 2002, [Fig. 2]). Surtout, une autre particularité, très marquée, à la fois politique et juridique, éclaire le développement qui précède : les communes sont propriétaires de la plupart des édifices religieux, qui sont très majoritairement des églises de culte catholique (soit approximativement 40 000 édifices). C’est là une situation qui, par elle-même, est profondément « anormale » : il n’est pas « normal » qu’une collectivité publique soit propriétaire d’un édifice du culte en régime de Séparation des Églises et de l’État, dont la logique aurait même voulu l’inverse, c’est-à-dire que les édifices du culte soient remis aux :ref:`fidèles` de chaque culte concerné, à charge pour eux de les entretenir. L’histoire qui est la nôtre explique cette situation. Le législateur de 1905 ne pouvait « abandonner aux associations cultuelles cette propriété », « diminuer ainsi le patrimoine de la société toute entière au profit de certains » (Briand, p. 266). L’État demeure donc propriétaire des :ref:`cathédrales <cathédrale>`, les communes des autres édifices du culte (art. 12 L. 1905). En outre, la loi du 13 avril 1908 a transmis aux communes la propriété des édifices appartenant aux anciens établissements publics, en l’absence d’:ref:`associations cultuelles <association cultuelle>` catholiques.

Les communes sont ainsi devenues propriétaires d’un immense patrimoine religieux puisque dans toutes les communes au moins une église avait été édifiée au fil du temps. Cette situation, devenue « naturelle » depuis 1905, a fini par soulever nombre de problèmes. Le problème le plus important, le plus courant, est celui de l’entretien de ces édifices. Près de la moitié d’entre eux a fait l’objet d’une protection au titre des :ref:`monuments historiques`. Il faut signaler d’ailleurs que cet entretien est indirectement obligatoire pour les collectivités propriétaires, donc, le plus souvent, les communes (voir CE, 10 juin 1921, `Commune de Monségur <https://nakala.fr/10.34847/nkl.9a5aw38n>`_ : *Lebon* p. 573) : l’absence, volontaire ou involontaire d’entretien, pourrait par ailleurs avoir pour conséquence de porter atteinte à l’exercice du culte, si l’occupation de l’édifice devenait dangereuse de ce fait. Les communes doivent donc aller à la « quête » des subventions pour pouvoir entretenir et réparer ces édifices du culte.

D’autres problèmes sont apparus, notamment celui de la conciliation entre la préoccupation légitime de la commune de « valoriser » son patrimoine et l’exigence encore plus légitime des croyants de disposer de leur lieu de culte. Une église est, par exemple, un lieu privilégié pour organiser des concerts, mais encore faut-il l’:ref:`accord préalable de l'affectataire cultuel`, et la frontière entre le concert religieux et celui qui ne l’est pas est difficile à fixer.

Le patrimoine religieux des collectivités territoriales — principalement des communes — soulève aujourd’hui une double question. La première est de savoir s’il convient de laisser la propriété, ou la gestion de ce patrimoine religieux aux communes, la seconde est de savoir quelle est l’utilisation qui peut être faite des édifices religieux en dehors des cérémonies cultuelles.

**Jean-Marie Pontier**

Bibliographie
--------------

* Briand Aristide, *Rapport fait le 4 mars 1905 au nom de la commission relative à la séparation des Églises et de l’État*.

* Ferstenberg Jacques, Priet François, Quilichini Paule, *Droit des collectivités territoriales*, Dalloz, 2 éd. 2016.

* Gimpel Jean, *La révolution industrielle du Moyen Âge*, Seuil, Points Histoire, 1975.

* Glaber Raoul, *Histoires*, *texte traduit et présenté par Mathieu* *Arnoux*, Turnhout, Brepols, 1996.

* Pontier Jean-Marie, Ricci Jean-Claude, Bourdon Jacques, *Droit de la culture*, Dalloz 2 éd. 1996.

* Pontier Jean-Marie, *La protection du patrimoine culturel*, Paris, L’Harmattan, 2019.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.5a56501x>`_



