:orphan:
.. index::
   single: bénitier
.. _bénitier:


===============
bénitier
===============

→ :ref:`domaine public mobilier` ; :ref:`mobilier liturgique` ; :ref:`objet liturgique`

Approches historique et religieuse
==================================

Récipient destiné à contenir l’eau bénite utilisée dans différents rituels chrétiens, le bénitier fait partie du mobilier liturgique incontournable des lieux de culte, mais aussi des objets domestiques du quotidien pendant des siècles.

Parmi les bénitiers liturgiques, l’on distingue le bénitier fixe du bénitier portatif. Le bénitier fixe se place à l’entrée de l’église — ou de la :ref:`chapelle` —, il permet aux :ref:`fidèles` de se signer à leur arrivée dans les lieux [Fig. 1 et Fig. 2]. Le bénitier portatif est un vase muni d’un pied utilisé pour les aspersions et bénédictions rituelles. Il n’existe aucune prescription concernant sa forme et la qualité sinon qu’il doit être doublé intérieurement d’une feuille d’étain ou de plomb ou d’une cuve de faïence. L’eau bénite qu’il contient est répandue sur les fidèles et les objets par un goupillon [Fig. 3]; l’eau peut être aussi dispersée à l’aide d’un rameau feuillu. La distinction se prolonge sur le plan juridique : les bénitiers fixes, scellés au sol, à un pilier ou un mur, « lorsqu’ils ne peuvent être détachés sans être fracturés ou détériorés, ou sans briser ou détériorer la partie du fonds à laquelle ils sont attachés » (art. 525 C. Civ.) appartiendront à la catégorie des immeubles par :ref:`destination`, par attache à perpétuelle demeure, tandis que le bénitier portatif fait partie des biens meubles (art. 528 C. Civ.). La même distinction vaut pour les bénitiers domestiques. Le vocabulaire patrimonial distingue les bénitiers des bénitiers d’applique [Fig. 4] selon leurs dimensions. Les premiers sont « réservés » aux églises tandis que les seconds correspondent à une « petite vasque appliquée et accrochée au mur ou à un meuble, utilisée dans une sacristie ou chez un particulier » (Vergain, Duhau, p. 34).

Le bénitier domestique désigne un récipient où est conservée l’eau bénite. Des réserves d’eau bénite dans les maisons particulières existent dès l’époque paléochrétienne. Au Moyen Âge, l’eau est conservée dans des seaux. Des récipients plus spécifiques se constituent à la fin du XV\ :sup:`e` siècle, qui suivent les modes décoratives. Le dosseret (partie verticale) se développe, il sert à une suspension murale, en même temps qu’il permet un décor plus important et reflet des goûts de l’époque. Dans d’autres civilisations christianisées, il existe des récipients de formes différentes. Dans les provinces chinoises, les missionnaires notent une énorme consommation d’eau bénite, que les fidèles viennent chercher dans des « vases de porcelaine attachés au bras par un bracelet » (Duteil, 1999, p. 69).

Le prodigieux succès des bénitiers répond à un recours très fréquent à l’eau bénite qu’il est essentiel d’avoir sous la main en cas d’urgence, par exemple pour protéger contre un orage ou la grêle. L’eau doit encore rester accessible en cas de maladie, sa vertu prophylactique étant largement exploitée. Du point de vue spirituel, les indulgences attachées à l’eau bénite rendent son usage biquotidien : un simple signe de croix double le nombre des jours d’indulgences obtenus s’il est fait les doigts humides. Dans chaque maison, une réserve d’eau bénite est conservée dans divers récipients. On a noté la présence constante des bénitiers dans les chambres, près du lit. On en trouve de nombreuses représentations sur des lithographies, des gravures, des :ref:`peintures <peinture>`, comme *Le viatique en Vendée* (François Brillaud, 1910) ou *Les pauvres gens* (Léonie Humbert-Vignot, 1908) mais surtout sur de nombreux :ref:`ex-voto`. Plus rarement, comme en Lorraine, le bénitier est suspendu au chambranle de la porte de la chambre, orné, comme partout, de buis béni ou de saule Marceau ; parfois une encoche est ménagée dans le dosseret pour maintenir le rameau béni servant à disperser l’eau bénite. Assez rarement en France, le bénitier figure dans la salle commune, c’est le cas en Queyras. En Montbrisonnais, ils sont placés sur la planche au-dessus de l’évier. En Pays basque, il existe des bénitiers domestiques fixes, occupant une niche ou insérés dans le mur, comme dans les églises. Les hauteurs moyennes s’échelonnent de 167 à 328 millimètres, même si certaines maisons lorraines conservent un bénitier de plus grande taille avec un important dosseret. Les plus grands des bénitiers domestiques peuvent aussi servir dans les sacristies, rarement dans les églises. 

Le bénitier est de si bonne vente que bien peu de fabriques de céramique négligent ce produit, d’autant qu’elles conçoivent des objets religieux voisins comme à Sarrebourg et Forbach. Comme pour l’ensemble de la céramique, la fabrication augmente aux XVIII\ :sup:`e` et XIX\ :sup:`e` siècles. La vente est souvent effectuée par des colporteurs ambulants, dans des foires et dans le commerce habituel. L’écoulement fléchit avec l’urbanisation et la déchristianisation, la date de rupture étant, comme souvent, la Seconde Guerre mondiale. Le déclin est accéléré par l’épuisement ou l’éloignement des sites argileux, par le renchérissement du bois, par la concurrence des chemins de fer qui désertifie les routes nationales commerçantes et par le déclin général de l’artisanat. Seule la Bretagne conserve sa faïence stannifère, disparue partout ailleurs ; le bénitier est le cadeau rituel du retour de Pardon. Le style breton se singularise face à l’industrialisation. Notamment à Locmaria les bénitiers continuent à se vendre sur place puis reviennent au goût du jour avec la tendance esthétique archaïsante et, dès lors, leur diffusion devient nationale.

La céramique est de loin le matériau le plus répandu aux XVIII\ :sup:`e` et XIX\ :sup:`e` siècles, susceptible de répondre aux plus pauvres des clients, comme aux plus riches qui préfèrent la porcelaine. Il existe cependant des quantités d’autres matériaux propres à satisfaire ce programme relativement simple : une cuve imperméable et un décor vertical comportant une perforation de suspension. On trouve des bénitiers en métal — précieux ou non —, joaillerie, ivoire, pierre dure, marbre, albâtre, onyx, nacre, verre, bois, cristal, os, corne, étoffe… ensuite viennent les matériaux industriels du XX\ :sup:`e` siècle.

Les bénitiers en verre, souvent blanc, tiennent une place à part. Les premiers repérés en France proviennent de Lorraine (1536). L’influence vénitienne s’implante et demeure, en particulier au XVIII\ :sup:`e` siècle. Les fabriques les plus connues sont à Nevers, Orléans, en Margeride, à Chaillot à Paris, en Alsace (Brünstadt), en Normandie et en Hautes-Pyrénées (à Gourgues, dont les bénitiers sont cependant jugés médiocres), en Argonne (dans le Binois), dans le Périgord et l’Agenais. Certains musées français ont une importante collection de bénitiers en verre (Bordeaux, Clermont, Aurillac, Châlons-sur-Marne, Mulhouse, Angers, Montrottier…).

Il existe des bénitiers insérés au pied d’un retable, dit retable-bénitiers, constituant comme un petit autel ou lieu de dévotion portatif. En région confolentaise et en Montbrisonnais, on trouve des cruchons en grès, percés dans leur flanc d’un trou permettant tout juste le passage de l’index, on y plonge le doigt avant la prière. À Ligron, on fabrique les deux sortes de bénitiers, appelées « à cuve ouverte », la plus habituelle, l’autre, dont on conserve un exemplaire de 1765, est appelé « à cuve fermée », d’un accès étroit, souvent en biais, laissant passer quelques doigts seulement. Ces dispositions dénotent un souci d’économie et d’hygiène : ne mouiller que le doigt utile et préserver l’eau des pollutions.

L’essentiel du décor est en général situé sur un dosseret en forme de plaque, mais parfois constitué par une sculpture. La plus simple est une :ref:`croix`. Ce type qui réunit deux objets en un seul, un crucifix et un bénitier, ne semble pas très courant. Les dosserets comportent de très nombreux motifs décoratifs variés, des arbres, des fleurs, des décors géométriques, des enfants, des têtes d’ange, le Christ, la Vierge, le Sacré-Cœur de Jésus ou celui de Marie, des anges gardiens, des saints. Les bénitiers patronymiques existent partout, mais on ne peut préciser leur succès réel.

**Bernard Berthod**

 

Bibliographie
--------------

* Chaperon Henri, *Les bénitiers domestiques et la dévotion dans l’art populaire*, Paris, Varia, 1986.

* Demeufve Georges, « Quelques bénitiers populaires lorrains », *in* Adolphe Riff (dir.), *L’art populaire en France*, Paris, Strasbourg, Istra, 1933, p. 115-123.

* Duteil, Jean-Pierre, « Reliques et objets pieux dans les communautés chrétiennes d’Asie, XVII\ :sup:`e` et XVIII\ :sup:`e` siècles », dans Édina Bozoky et Anne-Marie Helvétius (dir.), *Les reliques : objets de culte et symboles*, Turnhout, Brepols, 1999, p. 65-77.

* Guibal Jean, *Les objets de la vie quotidienne dans les Alpes*, Grenoble, Glénat, 1990.

* Berthod Bernard, Hardouin-Fugier Élisabeth, *Dictionnaire des objets de dévotion dans l’Europe catholique*, Paris, L’Amateur, 2006.

* Vergain Philippe, Duhau Isabelle (dir.), *Thésaurus de la désignation des objets mobiliers*, Inventaire général du patrimoine culturel — Ministère de la Culture et de la Communication, 2014.





`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.90acyg71>`_



