:orphan:
.. index::
   single: clochette
.. _clochette:


===============
clochette
===============


→ :ref:`cloche` ; :ref:`liturgie` ; :ref:`messe` ; :ref:`procession` ; :ref:`objet liturgique`

Approches historique et religieuse
====================================

Sous ce terme, l’on trouve des objets sonores ayant des formes et des usages variés, au-delà du champ cultuel. Outre la taille comme critère de distinction par rapport à la cloche, la clochette est un instrument portable ou portatif, mu ou agité à la main à l’aide d’un manche ou d’une poignée, permettant une utilisation autonome. Une clochette s’éloigne parfois de la forme campaniforme type pour ressembler aux clarines ou à un timbre hémisphérique. Il existe aussi des ensembles de clochettes qui constituent des instruments en tant que tels (carillon d’:ref:`autel`, rouet liturgique).

Dans le christianisme, les premières clochettes semblent être celles utilisées par les missionnaires celtes et irlandais pour signaler leur arrivée, comme celle conservée à Rocamadour (VI\ :sup:`e` siècle). Ce sont des modèles réduits fondus selon les mêmes procédés que les cloches d’église, également munies de massette captive. Il faut distinguer ces pièces des cloches issues de feuilles de tôle de fer pliée au marteau et soudée. Au Moyen Âge, la clochette est associée aux moines quêteurs ainsi qu’aux Antonins, chanoines réguliers formant l’ordre des Frères hospitaliers de Saint-Antoine.

La clochette liturgique a la forme d’une cloche en réduction, munie d’un manche métallique soudé à la clochette ou d’un manche en bois enserrant la palette (clochette ou sonnette d’autel ou encore sonnette liturgique, sonnette de messe) [Fig. 1]. Ce peut être aussi un instrument comportant trois à quatre clochettes ou plus, appelé aussi carillon d’autel. La clochette accompagne souvent les burettes sur leur plateau où un emplacement est prévu pour elle. Ces accessoires de la :ref:`liturgie`, pas toujours datés, d’aspect sobre ou au contraire richement décorés, en bronze ou en argent doré, constituent de véritables pièces d’orfèvrerie.

L’instrument donne la plupart des signaux s’adressant à l’assemblée des :ref:`fidèles` : il est généralement agité par l’enfant de chœur ou le servant de :ref:`messe` afin de les inviter à changer de posture (debout, assis, à genoux, prosternation…) lors d’une phase importante de l’office (lors de la sortie du clergé de la sacristie puis lors de la consécration des saintes espèces…, mais aussi au Sanctus, au Gloria, au Pater, à l’Agnus Dei...). En-dehors de la messe, lors de la cérémonie du Salut du Saint-Sacrement, une sonnerie marque le moment de l’adoration de l’Hostie dans l’ostensoir. Ce signal entendu à travers la :ref:`nef`, donc au-delà du :ref:`chœur` parfois fermé par une grille ou un jubé, faisait en sorte que tous accomplissent de façon coordonnée ou simultanée les gestes rituels. Cette pratique a en partie disparu avec les modifications liturgiques du :ref:`concile Vatican II` et les clochettes n’ont pas toujours été préservées. Quelques-unes se trouvent dans les :ref:`musées <musée religieux>`, beaucoup chez les collectionneurs.

Pendant le Triduum pascal (les trois jours précédant la célébration de Pâques par l’Église catholique), une longue sonnerie, souvent renforcé par les cloches de l’église, accompagne le chant du Gloria durant la messe *in cena Domini* du Jeudi saint ; ensuite on utilise des cloches en bois et parfois des clochettes montées sur roue (comme à Andorre) pour appeler aux offices lorsque les cloches restent muettes pendant ces trois jours. Le rouet liturgique, ou roue à clochettes, dont l’appellation varie d’une région à l’autre (rottler, roue à carillon, rouelle, treizain, crécelle…) est d’un usage attesté dès le IX\ :sup:`e` siècle [Fig. 2]. Sa conception est assez simple : une roue similaire à celle d’une charrette, des clochettes de tailles variables, mais semblables à celles utilisées au cou des animaux, un cadre ou un appareillage permettant de solidariser l’instrument au mur, un axe de rotation se prolongeant par une manivelle en métal au bout de laquelle est accrochée une cordelette. L’emplacement dans l’:ref:`édifice religieux` est principalement en partie haute (2 à 8 mètres) au niveau du chœur côté Épitre ou côté Évangile selon les lieux. Le nombre de clochettes varie de sept à treize selon les régions, en général un nombre symbolique. Cet instrument, rarement daté, est parfois considéré comme un archétype des carillons mélodiques à tambour qui actionnent les carillons d’horloge. Il est difficile d’affirmer que ces rouets étaient présents dans la plupart des églises, tant les remaniements ont été nombreux à travers les siècles, mais il en subsiste encore dans plusieurs régions. L’inventaire mené en 2009-2010 par la Société française de campanologie en dénombrait soixante-huit en place (parfois incomplets ou hors d’usage, parfois restaurés) répartis sur une quinzaine de départements, plus quelques exemplaires dans les musées ou des collections privées. L’usage de ces rouets est similaire à celui des sonnettes d’autel.Hors des édifices religieux, les clochettes connaissent différents usages. Les clochettes prennent place dans les diverses :ref:`processions <processions>`. Les enfants de chœur les agitent devant la procession des Rogations et lors du transport du viatique. Les clochettes de procession, appelées aussi tintenelles, diffèrent peu, dans leur forme, des sonnettes d’autel en bronze. Des clochettes sont accrochées sur certaines croix de procession, par exemple en Bretagne, à Guengat. Les croix de procession à clochettes, fréquentes en Pays basque, indignèrent au XVII\ :sup:`e` siècle l’inquisiteur Pierre de Lancre qui en comparait le bruit à une mascarade de village. Peut aussi être présent dans les processions le *tintinnabulum*, insigne de :ref:`basilique`, composé d’une clochette montée sur un cadre et portée par une hampe [Fig. 3]. *Tintinnabulum* et *ombrellino* sont, en principe, présents dans toutes les basiliques mineures, de part et d’autre de l’autel.

Les clochettes sont souvenir de :ref:`pèlerinage`, servant à attirer l’attention, à exprimer la joie et à accompagner des pipeaux, sifflets ou autres instruments. Certaines clochettes portent des inscriptions de localisations ou dénominations : « la petite margo ou la belle margo », « Ave maria », ou un nom de lieu. Elles sont très nombreuses en Angleterre. Certains sites de pèlerinages vendent des clochettes marquées de l’image vénérée. Les *campana thome* (cloches de Thomas de Cantorbéry), trempées dans les eaux du Jourdain, protègent contre la tempête, celles d’Altötting (Bavière) chassent les orages et les démons et retrouvent les vaches égarées. Plusieurs cloches ou clochettes sont vénérées comme :ref:`reliques <relique/reliquaire>`, telles la cloche dite de saint Patrick et son reliquaire, conservée au Musée national de Dublin, et la clochette dite bonnet de saint Mériadec de Stival, conservée dans l’église Saint-Mériadec-de-Stival à Pontivy (Morbihan) et réputée guérir les sourds et les malentendants [Fig. 4]. Certaines roues liturgiques sont parfois dotées de pouvoirs miraculeux ou guérisseurs, comme la roue à clochettes située dans l’église Notre-Dame de Confort à Confort-Meilars (Finistère).

Les clochettes ont occupé une place de choix dans les pratiques funéraires. Dans de nombreuses régions, les clochettes sont agitées par le clocheteur des morts ou crieur des morts ou encore « bannisseur des morts ». Payé par la famille, il parcourt la ville après un décès non pour convier à l’enterrement, mais à la veillée. En 1495, l’évêque de Nantes ordonne des prières pour les défunts à chaque minuit « à haute et intelligible voix ». À Avallon (Yonne) depuis le XVI\ :sup:`e` siècle, elle sert au crieur des trépassés, qui rappellent tous les dimanches qu’il faut prier pour les morts. Dans le Cotentin, l’enfant agite la sonnette en devançant les convois mortuaires pour laisser aux gens le temps d’arriver. Les confréries de la bonne mort accompagnent le corbillard en faisant sonner des clochettes. Il en est de même des clochettes des trépassés et des tintenelles de chariton (:ref:`confrérie` de charité en Normandie). À l’origine, elles servaient à éloigner les passants lors de l’enterrement d’un contagieux. Le clocheteur des trépassés ou le tintenellier, en tête du cortège funèbre, agitait deux tintenelles afin d’avertir les passants et de rythmer le pas. Cette :ref:`tradition` perdure dans le département de l’Eure.

Enfin, l’association de petites clochettes à des vêtements est fort ancienne : on évoque déjà dans la :ref:`Bible` celles que portait la tunique du grand-prêtre Aaron. Parmi les objets utilisés par l’Église catholique, en tant qu’éléments sonores, il est courant dans certaines régions ou à certaines époques de trouver des petites clochettes ou des grelots sur les croix de procession, ou encore en accompagnement de bannières, plus rarement sur les chapes (pluvials) portées par un officiant.

**Bernard Berthod, Éric Sutter**

 

Bibliographie
--------------

* Fabre Charles, Sutter Éric, *Inventaire des roues à clochettes encore existantes en France*, La Garenne-Colombes, Société française de campanologie, 2011.

* Morillot Lucien, *Étude sur l’emploi des clochettes chez les anciens et depuis le triomphe du christianisme*, Damongeot et Cie, Dijon, 1888.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.f9cd3k2f>`_





