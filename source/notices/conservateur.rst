:orphan:
.. index::
   single: conservateur
.. _conservateur:


=============================
conservateur du patrimoine
=============================


→ :ref:`architecte` ; :ref:`édifice religieux`

Selon l’article L. 442-8 du code du patrimoine, « les activités scientifiques des musées de France sont assurées sous la responsabilité de professionnels présentant des qualifications définies par décret en Conseil d’État ». Ces professionnels sont les conservateurs du patrimoine.

On distingue deux catégories de conservateurs du patrimoine selon la catégorie de collectivité de rattachement, les conservateurs du patrimoine, qui relèvent de l’État et ont précédé, historiquement, les seconds, ces derniers étant les conservateurs territoriaux du patrimoine. Il était logique qu’avec le développement de la décentralisation territoriale il y eût l’équivalent, dans les collectivités territoriales, des corps d’État. Cela s’imposait d’autant plus que si ce sont les institutions culturelles nationales qui sont les plus connues, celles des :ref:`collectivités territoriales` sont, de loin, les plus nombreuses, ce que l’on appelle le « tissu culturel » est constitué avant tout de ces institutions culturelles locales.

Dans les deux cas, qu’il s’agisse de l’État ou des collectivités territoriales, il s’agit de fonctionnaires, fonctionnaires d’État dans le premier cas, fonctionnaires territoriaux dans le second. Les premiers appartiennent à des « corps » tandis que l’on parle, pour les seconds, de « cadres d’emplois ». Certains emplois des collectivités territoriales sont occupés ou peuvent être occupés par des personnels d’État mis à disposition. Ainsi, selon l’article L. 212-9 du code du patrimoine, les directeurs des services départementaux d’archives sont choisis parmi les conservateurs généraux du patrimoine de l’État.

L’institution d’un « corps » (c’est-à-dire, donc, d’un cadre d’emplois) de conservateurs territoriaux du patrimoine par le décret n° 91-839 du 2 septembre 1991 a donné lieu à un contentieux assez abondant, avec les demandes d’intégration dans ce cadre de personnes estimant pouvant y prétendre, compte tenu des textes, avec notamment la question de l’homologation des diplômes ou de la formation suivie (V. par ex., au sein d’un contentieux fourni, CE, 29 déc. 1997, avec plusieurs arrêts du même jour, V. n° 171062 ; CE, 28 juill. 2000, n° 141437 ; CE, 1 juin 2001, n° 201549 ; CE, 22 juin 2001, n° 202455 et 202458).

S’agissant des conservateurs du patrimoine de l’État, les textes ont varié dans le temps, l’adoption d’un nouveau statut entraînant systématiquement des recours (sur une contestation relative aux modalités de recrutement des conservateurs du patrimoine de l’État, V. CE 4 mai 2016 n° 394722). Selon l’actuel article 1 du décret n° 2013-788 du 28 août 2013 portant statut particulier du corps des conservateurs du patrimoine les conservateurs du patrimoine constituent, « un corps supérieur à caractère scientifique et technique et à vocation interministérielle ».

La formation des conservateurs du patrimoine est identique, qu’il s’agisse des conservateurs d’État ou des conservateurs territoriaux du patrimoine. Cependant, afin de respecter le principe de libre administration des collectivités territoriales, le recrutement des conservateurs territoriaux du patrimoine est effectué par l’autorité locale propriétaire des collections qui doit choisir, parmi les conservateurs reçus à un concours organisé par le Centre national de la fonction publique territoriale (CNFPT) en liaison avec l’Institut national du patrimoine et inscrits, à ce titre, pendant deux ans, sur une liste d’aptitude.

Les conservateurs du patrimoine exercent, quel que soit leur grade, des responsabilités scientifiques et techniques visant notamment à inventorier, récoler, étudier, classer, conserver, entretenir, enrichir, mettre en valeur et faire connaître le patrimoine. Ils peuvent participer à cette action par des enseignements ou des publications. Ils peuvent être appelés à favoriser la création littéraire ou artistique dans leur domaine de compétence particulier. Ils exercent notamment leurs fonctions dans des services déconcentrés, des services de l’administration centrale, des services à compétence nationale ou des établissements publics. En outre, « ils peuvent se voir confier des missions particulières portant sur l’ensemble du territoire ou sur une zone géographique déterminée. Ces missions peuvent avoir un caractère administratif, scientifique, technique ou pédagogique Ils participent au développement de la recherche. Ils ont vocation à exercer des fonctions de direction des établissements ou services assurant les missions » précédemment indiquées (Décret n° 2013-788 du 28 août 2013 portant statut particulier du corps des conservateurs du patrimoine, art. 3). Quant aux conservateurs en chef du patrimoine et les conservateurs généraux du patrimoine, ils « peuvent, en outre, être chargés des fonctions d’encadrement supérieur, de coordination ainsi que de conseils ou d’études comportant des responsabilités particulières. Ils peuvent être chargés, par arrêté du ministre de la culture, de missions d’inspection générale. » (*Ibid*, art. 4).

Les conservateurs généraux du patrimoine sont chargés de hautes responsabilités scientifiques et techniques en matière de conservation du patrimoine. Ils ont vocation à assurer la direction de services centraux, de services déconcentrés, de services à compétence nationale ou de grands établissements relevant de leur compétence.

Lors de leur titularisation, les conservateurs sont affectés, par arrêté du ministre chargé de la culture, dans l’une des spécialités suivantes : archéologie ; archives ; monuments historiques et inventaire ; musées ; patrimoine scientifique, technique et naturel. La spécialité d’affectation est identique à la spécialité dans laquelle ils ont été admis à suivre leur formation à l’Institut national du patrimoine. Les conservateurs du patrimoine peuvent, en cours de carrière, demander à être affectés dans un emploi correspondant à une autre spécialité que celle qui leur a été attribuée lors de leur première affectation.

Les membres du corps des conservateurs du patrimoine ne peuvent se livrer, directement ou indirectement, au commerce ou à l’expertise d’œuvres d’art et d’objets de collection. Ils peuvent néanmoins être autorisés par le ministre à procéder à des expertises ordonnées par un tribunal ou à donner des consultations à la demande d’une autorité administrative.

La charte de déontologie des conservateurs du patrimoine présente des spécificités notables. La charte fait référence à un document international, le code de déontologie de l’ICOM le conseil international des musées, ONG reconnue par l’UNESCO et qui a élaboré dès 1981 une première version de son code de déontologie). En ce qui concerne les principes généraux de la charte, celle-ci rappelle que la mission des conservateurs est une mission de service public « qui vise au rayonnement du patrimoine naturel et culturel de l’humanité ». Dans ces principes généraux, on relève deux indications intéressantes. La première est la possibilité pour le conservateur de manifester à l’autorité administrative et la personne morale qui l’emploie « son opposition à des pratiques qui lui paraîtraient nuire au musée ou à la profession et contraires à la déontologie professionnelle » et « il est du devoir des autorités dirigeantes et de tutelle des musées de France de prendre en compte cette manifestation pour éclairer leur décision ».

La seconde, à laquelle on s’attend, concerne le commerce et l’expertise. « Le conservateur s’interdit de se livrer directement ou indirectement au commerce ou à l’expertise d’œuvres d’art et d’objets patrimoniaux. Le conservateur est néanmoins autorisé par le ministre ou les autorités de tutelle à procéder à des expertises ordonnées par un tribunal ou à donner des consultations à la demande d’une autorité administrative après information de l’autorité hiérarchique de l’établissement dont il relève ».

De même, encore, « Le conservateur s’interdit toute contribution à des catalogues de ventes ou de galeries et plus généralement tout apport scientifique ou intellectuel pouvant valoir expertise commerciale. Il s’interdit également de recommander de manière exclusive un marchand commissaire-priseur ou un expert et plus généralement un quelconque prestataire ». Les conservateurs s’interdisent également d’utiliser à des fins personnelles les collections du musée (la tentation d’utilisation à des fins personnelles de meubles au sens juridique qui sont des œuvres d’art ou des objets d’art ne vaut pas seulement pour les conservateurs du patrimoine…).**Jean-Marie Pontier**

Bibliographie
--------------

* France. Ministère de l’Intérieur, Ministère de la Culture et de la Communication, *Le rôle du conservateur de monument historique dans les cathédrales appartenant à l’État* (Fiches pratiques pour les cathédrales, n° 3), 2014 : `https://www.culture.gouv.fr/Thematiques/Monuments-Sites/Ressources/Les-fiches-pratiques <https://www.culture.gouv.fr/Thematiques/Monuments-Sites/Ressources/Les-fiches-pratiques>`_

* Pontier Jean-Marie, *La protection du patrimoine culturel*, Paris, L’Harmattan 2019.

* Poulard Frédéric, Depuis Marie-Aude, `Les conservatrices et conservateurs du patrimoine en France. Enquête auprès des professionnel.le.s formé.e.s par l’Institut national du patrimoine (promotions 1991 à 2019), <https://hal.science/hal-03904770/>`_ Clersé, 2022, hal-03904770.`Télécharger la notice (PDF) <http://dx.doi.org/10.34847/nkl.ee9e298k>`_



