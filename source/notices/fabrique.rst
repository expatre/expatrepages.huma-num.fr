:orphan:
.. index::
   single: fabrique
.. _fabrique:


===============
fabrique
===============


→ :ref:`Articles organiques` ; :ref:`concordat de 1801` ; :ref:`entretien/réparations` ; :ref:`inventaires` ; :ref:`travaux`

Présentation générale
=====================

Dès l’époque médiévale et sous l’Ancien Régime, des fabriques étaient chargées de la gestion du temporel d’une :ref:`paroisse` ; elles s’occupaient des réparations des bâtiments, du mobilier, plus largement de l’ensemble des ressources et des dépenses des paroisses.

Instaurant le régime des cultes reconnus en 1802, Bonaparte entendait que l’État contrôlât la gestion de leurs biens, qu’il s’agisse de ceux consacrés au :ref:`culte` ou utilisés pour le culte (ornements, bâtiments des églises, cimetières…), ou des biens temporels destinés à la subsistance des clercs et au fonctionnement du culte. Des établissements publics du culte de divers types seront créés, compétents pour gérer ce patrimoine dans le cadre d’une stricte tutelle administrative. Tels furent les menses épiscopales et curiales, les fabriques des paroisses, les conseils presbytéraux, les :ref:`consistoires <consistoire>`… L’article 76 des :ref:`Articles organiques <articles organiques>` prévoit qu’il « sera établi des fabriques, pour veiller à l’entretien et à la conservation des :ref:`temples <temple>`, à l’administration des aumônes ».

Au lendemain du :ref:`concordat <concordat de 1801>`, les évêques organisèrent des « fabriques intérieures », dont les règlements furent homologués par arrêté du 9 floréal an XI, chargées de gérer les recettes et dépenses ordinaires du culte, essentiellement les dons pour la décoration de l’église. Parallèlement, les préfets mirent en place des « fabriques extérieures » (arrêté du 7 thermidor an XI) auxquelles on restitua les biens non aliénés pendant la Révolution, et chargées d’administrer les recettes et dépenses extraordinaires des églises. Les fabriques des paroisses sont organisées par le décret du 30 décembre 1809 qui fusionne les deux catégories. Leur administration est assurée par deux instances : un conseil de fabrique, composé du curé ou du desservant, du maire et de 5 à 9 élus parmi les paroissiens et un bureau des marguillers, comprenant le curé ou desservant et trois membres du conseil. Dans ces deux instances, le curé ou desservant a la première place et les notables locaux siègent dans le conseil. L’évêque a autorité sur le curé ou desservant et contrôle les comptes. Les fabriques ne se généraliseront dans chaque paroisse qu’au cours des années 1830.

La loi du 28 décembre 1904 retirera aux fabriques le service des funérailles et en confèrera le monopole aux communes. Un an plus tard, les fabriques disparaitront en application de l’article 2 de la :ref:`loi du 9 décembre 1905` aux termes duquel « Les établissements publics du culte sont supprimés ». Les biens de ces établissements, et donc des fabriques, devaient être transférés aux :ref:`associations cultuelles <association cultuelle>` prévues par la même loi. On sait que ce transfert ne se fera pas pour les biens ayant appartenu aux établissements publics du culte catholique dont les fabriques seront néanmoins supprimées. En :ref:`Alsace-Moselle`, les fabriques d’églises subsistent. Leur organisation a connu depuis quelques aménagements afin de moderniser la composition des conseils et le mode de gestion.

Aspects patrimoniaux
====================

Sous le régime des cultes reconnus, la constitution (ou la reconstitution) du patrimoine religieux s’opère, mais dans un cadre juridique défini dont les fabriques sont la pierre angulaire, du moins pour l’Église catholique. La fabrique est l’instance de gestion du patrimoine temporel de la paroisse et donc d’un patrimoine culturel en devenir, sans, à l’époque, en avoir toujours conscience (Fornerod, p. 281 et s) ; de fait, les fabriques seront expressément visées par la loi pour la conservation des monuments et objets d’art du 30 mars 1887 aux côtés de l’État, des départements et des communes.

Dans le silence du Concordat et des Articles organiques, deux avis du Conseil d’État de l’an XIII attribuaient la :ref:`propriété` des cathédrales et palais épiscopaux à l’État et celle des églises paroissiales aux communes. Par la suite, d’autres églises furent construites. Elles furent propriété de l’État, des communes ou des fabriques, en application de l’article 522 du code civil, et selon les circonstances (rôle de telle personne morale dans l’initiative du projet, son financement…). Au cours du XIX\ :sup:`e` siècle, l’administration des monuments historiques intervint également. Dans la première décennie du régime des cultes reconnus, la situation juridique demeure confuse, signe de la méfiance du gouvernement à l’égard desdites fabriques. Celles-ci profitèrent néanmoins des revenus de donations et fondations. Après discussion, elles récupérèrent les biens et rentes des anciennes fabriques qui n’avaient pas été aliénés.

Le décret de 1809 établit une règlementation d’ensemble appelée à durer. Il reprenait les dispositions des Articles organiques pour définir leurs compétences : « Les fabriques […] sont chargées de veiller à l’entretien et à la conservation des temples ; d’administrer les aumônes et les biens, rentes et perceptions autorisées par les lois et règlements, les sommes supplémentaires fournies par les communes, et généralement tous les fonds qui sont affectés à l’exercice du culte… » (art. 1). L’article 36 prévoit les ressources des fabriques : biens et rentes restitués en application des articles 72 et 74 de la loi de germinal an X, biens acquis par la suite avec autorisation du gouvernement, produits de la location des bancs et chaises, quêtes, revenus tirés de l’exercice du monopole des pompes funèbres, attribué aux fabriques par le décret du 23 prairial an XII… Ces opérations financières sont contrôlées par l’évêque, et non pas seulement par l’administration publique.

L’article 37 énumère quatre séries de dépenses à la charge de la fabrique et d’autres articles complètent cette liste : frais nécessaires au culte (« les ornements, les vases sacrés, le linge, le luminaire, le pain, le vin, l’encens… ») ; salaire des desservants, vicaires et de tous les employés de l’église. Elle veille à l’embellissement intérieur de l’église, entretient l’église, le presbytère et le cimetière.

L’article 92 indique les dépenses à la charge des communes et l’on mesure ici l’importance du :ref:`financement` public du culte. La commune fournit un presbytère ou logement, finance les grosses réparations et « supplée à l’insuffisance des revenus de la fabrique… » pour les charges que celle-ci doit assumer. Cette aide, que la commune est dans l’obligation d’apporter dans certaines circonstances, sera souvent accordée dans une collaboration sereine, mais elle sera aussi source de nombreux contentieux. Cette faveur financière accordée aux cultes reconnus illustre la condition de service public, contrôlé par l’État et place la fabrique dans une certaine dépendance par rapport à la commune. Le poste budgétaire pour lequel cette aide communale sera la plus fréquente est celui des grosses réparations, de l’église ou du logement du desservant, dépenses obligatoires pour la commune. Des devis sont établis et la demande de la fabrique fait l’objet d’un rapport auprès du préfet. On a un temps débattu la question de savoir si presbytères et cimetières bénéficiaient des mêmes aides communales que l’église. La loi municipale du 18 juillet 1837 (art. 30), puis celle du 24 juillet 1867 précisèrent les choses de façon favorable à l’Église. Ils inscrivaient, parmi les dépenses obligatoires de la commune, le logement des curés et desservants, les secours aux fabriques dans certaines circonstances, et les grosses réparations des édifices communaux ce qui ne se limite pas aux seules églises.

Cette relative générosité à l’égard des cultes reconnus ne fut plus de mise lors de la politique anticléricale de la III République. La loi municipale du 5 avril 1884 (art. 136) restreint l’obligation incombant aux communes, qui ne doivent plus subvenir à l’insuffisance des ressources des fabriques que pour les dépenses de grosses réparations des édifices du culte ou de logement du desservant. Dans les années suivantes, d’autres dispositions viennent encore accroître le contrôle des pouvoirs publics sur les budgets des fabriques (loi du 26 janvier 1892 et décret du 27 mars 1893). Dans la pratique, pendant la première moitié du XIX\ :sup:`e` siècle, ce ne furent que des sommes peu élevées qui furent employées à la réparation des églises, par les :ref:`collectivités territoriales` ou par les établissements publics du culte. Les fonds consacrés aux constructions ou aux réparations d’églises augmentèrent sous le Second Empire, pour diminuer vers la fin du siècle.

**Brigitte Basdevant-Gaudemet**

 

Bibliographie
--------------

* Aucoc Léon, *Des changements apportés depuis 1884 à la législation sur les fabriques des églises*, Paris, 1893.

* Fornerod Anne, *Le régime juridique du patrimoine culturel religieux*, Paris, L’Harmattan, 2013.

* Guilbaud Mathilde, « Les fabriques paroissiales rurales au XIX\ :sup:`e` siècle. L’exemple des campagnes de Seine-et-Marne », *Histoire &amp; Sociétés Rurales*, 2007/2 (Vol. 28), p. 67-88.

* Lacaze Julien, « La protection des cathédrales en tant que monuments historiques », *Revue historique de droit français et étranger*, 84 (4), 2006, p. 569-612. 

* *Répertoire Dalloz*, 1853, v° Culte.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.fe844s6k>`_



