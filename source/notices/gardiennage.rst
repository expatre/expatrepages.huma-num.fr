:orphan:
.. index::
   single: gardiennage
.. _gardiennage:


===============
gardiennage
===============


→ :ref:`affectataire cultuel` ; :ref:`entretien/réparations` ; :ref:`financement`

Approche juridique
==================

Les communes propriétaires d’édifices cultuels ont la responsabilité de l’:ref:`entretien <entretien/réparations>` et de la conservation* de leurs biens et peuvent à ce titre organiser cette «prestation facultative» (Circulaire du ministère de l’Intérieur du 29 juillet 2011 relatives aux édifices du culte, p. 28) qu’est le gardiennage. Aucun texte ne précise ce que recouvre exactement cette fonction, même si le gardiennage a pu être défini en doctrine comme la «surveillance de l’église du point de vue de sa conservation, ce qui suppose le passage régulier d’une personne pour suivre ce qu’il advient à l’édifice et à ce qu’il contient.» (Chapu, p. 82) ou encore comme «la surveillance de l’église, de ses meubles*, les soins apportés à leur entretien» (Kerlévéo, p. 223). Il peut intervenir en complément d’autres mesures prises par la commune afin «d’éviter les vols, les dégradations ou destructions* volontaires ou autres actes de :ref:`vandalisme` ou de pillage» (Rép. min. no 05365 : JO Sénat, 24 mars 1994, p. 635) et implique alors la désignation d’un gardien, par arrêté municipal. Le rôle prépondérant joué par l’:ref:`affectataire cultuel <affectataire cultuel>` dans l’utilisation et la fréquentation des lieux fait de ce dernier un gardien tout désigné et, de fait, la mission de gardiennage lui a été souvent confiée très tôt après la loi de séparation des Églises et de l’État.

En principe, cette fonction est indépendante des prérogatives de l’affectataire cultuel liées à la pratique du culte. Aussi le gardiennage peut-il être confié à une autre personne, solution privilégiée dans un contexte de diminution des clercs et de l’augmentation concomitante de leur charge pastorale. Pour autant, le gardiennage s’effectue en tenant compte de l’:ref:`affectation cultuelle` des lieux, c’est-à-dire dans le respect des horaires d’ouverture de l’église et de la tenue des activités religieuses qui sont de la responsabilité de l’affectataire. En conséquence, même si aucun texte ne l’impose formellement, il apparaît souhaitable que le maire obtienne l’:ref:`accord préalable de l'affectataire cultuel` sur la désignation et les missions du gardien (Circulaire du ministère de l’Intérieur du 29 juillet 2011, p. 28).

En outre, l’affectation cultuelle exige que « la jouissance d’un édifice affecté au culte catholique doit être faite conformément aux règles d’organisation générale de ce culte lesquelles comprennent la soumission à lahiérarchie ecclésiastique », ce qui limite la marge de manœuvre de la commune dans le choix du gardien. En conséquence, un arrêté municipal ne pouvait confier l’entretien et le gardiennage de l’:ref:`église paroissiale`, malgré l’opposition de l’évêque de Soissons, à l’association traditionaliste Regina Coeli qui n’observe pas ce principe (TA Amiens, 16 sept. 1986, *Labille c. commune de Villeneuve Saint-Germain*). La même règle s’applique à l’affectataire cultuel qui assume aussi un emploi de gardien : le maire doit retirer le gardiennage à l’affectataire qui s’est vu retirer sa charge pastorale — et donc la disposition de l’église paroissiale — par l’évêque (CE, 24 avril 1981, n 21418, *Cousseran*).

Un autre point de distinction avec les prérogatives de l’affectataire tient à la rémunération. La :ref:`loi du 9 décembre 1905` a mis fin à la rémunération des ministres du culte (art. 2). Or, l’article 5 de la loi du 13 avril 1908 (ayant modifié l’article 13 de la loi du 9 décembre 1905) permet à l’État, aux départements et aux communes d’« engager les dépenses nécessaires pour l’entretien et la conservation des édifices du culte dont la propriété leur est reconnue par la présente loi », fondant ainsi la possibilité de verser une indemnité au gardien. En effet, l’affectation cultuelle « ne saurait enlever à la commune le droit de pourvoir, à raison de sa qualité de propriétaire, à l’entretien des biens » et les sommes correspondant au « salaire de deux préposés au gardiennage, à la conservation et à l’entretien de deux églises et des objets mobiliers qui les garnissent […] ne rentrent pas dans les dépenses relatives à l’exercice du culte » (CE, 13 déc. 1912, *Commune de Montlaur: Lebon* p. 1194).

Il convient toutefois de veiller à ce que cette indemnité ne se transforme pas en une subvention publique au culte prohibée par l’article 2 de la loi du 9 décembre 1905 (CE, 16 mai 1919, *Commune de Montjoie : Lebon* p. 429). Une circulaire du ministère de l’Intérieur en fixe chaque année le — modique — plafond, notamment « afin de maintenir une certaine homogénéité sur l’ensemble du territoire et d’éviter des disparités trop grandes dans le montant des indemnités servies, cette rétribution qui ne saurait dépasser un niveau modeste sans changer de caractère doit correspondre approximativement à la réalité des prestations effectuées. » (Rép. Min. n 66602: *JO Ass. Nat.*, 4 oct. 2005, p. 9255). En outre, « la loi du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions, qui n’a pas remis en cause la jurisprudence relative à l’indemnité de gardiennage, n’a pas eu d’effet sur ce pouvoir de réglementation » du ministère de l’Intérieur, les :ref:`collectivités locales <collectivités territoriales>` ne pouvant en conséquence « bénéficier de la liberté totale de fixation du montant de l’indemnité de gardiennage des églises communales » (Rép. Min. n 66602: *JO Ass. Nat.*, 4 oct. 2005, p. 9255). Ce dernier point renvoie de nouveau à la question de l’étendue des tâches du gardien au regard de la modicité de sa rémunération. Si l’on considère qu’il ne s’agit pas « d’une présence constante, mais d’une visite régulière de l’église pour en surveiller l’état et rendre compte au maire des dégâts éventuellement constatés », il est possible, qu’en vertu de l’article 13 modifié de la loi de 1905 la commune emploie un préposé à l’entretien qui peut cumuler sa rémunération à ce titre avec l’indemnité de gardiennage ayant un objet différent » (Réponse du ministère de l’Intérieur à la question n 05365, *JO Sénat*, 12 mai 1994, p. 1169). Il s’agit là d’un aménagement supplémentaire de l’interdiction d’un :ref:`financement` public des cultes à considérer au vu de l’importance de l’entretien dans la préservation du patrimoine religieux, même si la notion d’entretien, qui n’est pas définie par la loi de Séparation, présente elle-même des contours assez flous lorsqu’il s’agit de déterminer les parts respectives de la personne publique propriétaire et de l’autorité affectataire.

**Anne Fornerod**


Bibliographie
--------------

* Chapu Christian, « Les églises communales rurales dans le cadre des paroisses actuelles », dans *Les églises rurales*, Actes du colloque de Lutz-en-Dunois en octobre 1996, Cahiers de Rencontre avec le patrimoine religieux, 1997, p. 79-83.

* Imbert Jean, « Introduction », dans Gaudemet Jean (dir.), *Administration et Église du concordat à la séparation de l’Église et de l’État*, Genève, Librairie Droz, 1987.

* Kerlévéo Jean, *L’Église catholique en régime français de séparation, tome 2, Les prérogatives du curé dans son église*, Paris, Desclée, 1956.



`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.f33bcxlz>`_



