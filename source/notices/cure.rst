:orphan:
.. index::
   single: curé
.. _curé:


===============
curé
===============


→ :ref:`affectataire cultuel` ; :ref:`église paroissiale` ; :ref:`fidèles` ; :ref:`hiérarchie` ; :ref:`paroisse`

Approche religieuse
===================

Le curé en :ref:`droit canonique` est une des plus importantes fonctions de l’organisation des :ref:`diocèses <diocèse>` de l’Église catholique. Il est placé à la tête d’une :ref:`paroisse` ou communauté déterminée de :ref:`fidèles` érigée en personne juridique pour laquelle il exerce la charge pastorale que l’Église confie à ceux qu’elle a ordonné prêtres. Le Code de droit canonique (can. 530) décrit cette charge en distinguant trois fonctions dont le curé porte la responsabilité : l’enseignement, dont font partie la catéchèse et la prédication, la sanctification avec l’activité de célébration des sacrements (baptême, mariage, eucharistie…) et de cérémonies concernant la vie chrétienne des fidèles (funérailles, bénédictions, « conduite des :ref:`processions <procession>` en dehors de l’église » …) et le gouvernement ou fonction de direction de la paroisse. Selon le canon 519, le curé exerce les fonctions de « pasteur propre » de la paroisse avec « la collaboration éventuelle d’autres prêtres et de diacres et avec l’aide apportée par des laïcs selon le droit ». Dans certains cas, des prêtres pourraient être regroupés en équipe pour exercer solidairement la charge pastorale sous la direction d’un modérateur (can. 517 § 1). 

Le Code de droit canonique lie d’une manière exclusive l’exercice des devoirs et droits de la fonction curiale et le statut personnel de prêtre car, selon l’expression traditionnelle, elle comporte « la pleine charge d’âmes » avec une activité au for externe et au for interne qui, pour une part, est sacramentelle. On pense par exemple à la faculté d’entendre les confessions que le curé reçoit en vertu de son office. Cette obligation est toutefois difficile à appliquer dans des situations où existe une pénurie de prêtres. Aussi, lorsque l’évêque diocésain déclare que le diocèse est dans cet état, le Code permet que, pour une ou des paroisses déterminées, les fonctions curiales qui ne demandent pas la réception du sacrement de l’ordre soient exercées par « un diacre ou une personne non revêtue du caractère sacerdotal ou encore une communauté de personnes » avec un prêtre « qui, muni des pouvoirs et facultés du curé, sera le modérateur de la charge pastorale » (can. 517 § 2). Ce modérateur a une fonction différente de celle mentionnée plus haut.

Cette figure juridique du modérateur s’est répandue en Europe, où le nombre des prêtres est insuffisant. En France, elle a conduit à la création d’équipes d’animation pastorale, comprenant diacres, laïcs et un prêtre modérateur. Le fait que des personnes non-prêtres reçoivent un rôle institutionnel dans une paroisse et participent à l’exercice de la charge pastorale a contribué au changement des relations entre clercs et laïcs et affecté la figure canonique traditionnelle de l’exercice de la charge de curé, même si, du point de vue juridique, l’organisation autorisée par le Code revêt un caractère provisoire lié à la permanence ou non d’une situation de pénurie. On a dû mieux distinguer dans la fonction de curé une part qui recouvre un ensemble d’activités pouvant être exercées par des personnes non ordonnées prêtres et une autre qui demande que le titulaire ait reçu les capacités, notamment sacramentelles, qui viennent de l’ordination.

Cette distinction ne s’est pas faite sans débats voire conflits car il fallait répartir des fonctions entre les membres non ordonnés des équipes et le modérateur, en particulier respecter et, mieux encore, redécouvrir les éléments d’organisation de l’Église essentiels qui fondent le caractère hiérarchique de la structure. Ainsi, en comparaison des principes qui expliquent l’organisation des paroisses dans les Églises protestantes, on devait motiver la nécessité qu’un ministre ordonné reçoive naturellement un office de direction auquel collaborent les diacres et les laïcs. De plus, le Code de droit canonique organise la responsabilité de l’action pastorale à partir du critère d’unité de décision, comme on le voit dans le cas d’un diocèse où l’évêque reçoit un office de pasteur qu’il exerce avec la collaboration de conseils et de personnes mais avec une capacité personnelle de décider. Comment équilibrer le concept de participation et celui de modération confiée à un prêtre ?

D’une manière générale, l’évolution de la conception des relations entre fidèles dans le sillage des textes du concile Vatican II avait déjà bouleversé la manière dont la fonction du curé s’exerçait dans les paroisses. Dans le Code de droit canonique révisé, après le deuxième concile du Vatican, deux institutions ont été introduites qui donnent un cadre à la participation des fidèles au sein de la paroisse. D’une part le conseil pastoral, dont la création dépend des circonstances concrètes du diocèse et du jugement de l’évêque diocésain, rassemble des fidèles de la paroisse qui apportent « leur concours pour favoriser l’activité pastorale ». Le curé y a le rôle de présidence sans toutefois que le Code de droit canonique le définisse, déclarant seulement que le conseil « ne possède que voix consultative ». D’autre part le conseil paroissial pour les affaires économiques, qui aide le curé dans sa charge d’administrateur des biens de la paroisse.

Pour le Code de droit canonique, le curé est le représentant légal de la paroisse dans toutes les affaires juridiques en application du canon 1279 § 1 : « L’administration des biens ecclésiastiques revient à celui qui dirige de façon immédiate la personne à qui ces biens appartiennent. » Aussi le canon 532 déclare-t-il qu’il lui revient de « veiller » à l’administration de ces biens en application de la législation qui leur est consacrée dans le livre V du Code de droit canonique, en particulier celle qui concerne les administrateurs (cann. 1281-1288). On y définit d’une manière générale les devoirs et les droits de ceux à qui revient une charge d’administration des biens. Ils doivent veiller à ce que ceux « qui leur sont confiés ne périssent pas et ne subissent aucun dommage », veiller aussi « à garantir par des moyens valides en droit civil la propriété des biens ecclésiastiques » et les administrer dans le strict respect du droit tant canonique que civil. Enfin, il est déclaré que l’administration des biens est soumise au gouvernement de l’évêque diocésain, qui intervient en cas de négligeance.   


**Patrick Valdrini**
 

Bibliographie
--------------

* Valdrini Patrick (avec Émile Kouveglo), *Leçons de droit canonique. Communautés, personnes, gouvernement*, Paris, Salvator, 2017.

* Abbal Élisabeth, *Paroisse et territorialité dans le contexte français*, Paris, Cerf, 2016.

* Borras Alphonse, *Les communautés paroissiales. Droit canonique et perspectives pastorales*, Paris, Cerf, 1996.

`Télécharger la notice (PDF) <https://nakala.fr/10.34847/nkl.bdb137k4>`_



