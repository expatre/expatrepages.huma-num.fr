========
À Propos
========

Le projet ExPatRe - *Explorer le patrimoine culturel religieux* consiste en l’élaboration d’une plateforme numérique pour accueillir un *Dictionnaire du patrimoine culturel religieux* en cours de rédaction.

Le patrimoine religieux soulève des questions sémantiques substantielles dès lors qu’il est abordé sous l’angle pluridisciplinaire. Le dictionnaire constitue alors l’outil idoine, car il permet de saisir et dépasser les variations d’acceptions qu’un même terme peut recouvrir d’une discipline à une autre, de relier différents concepts pour parvenir à la création d’un champ lexical propre au patrimoine culturel religieux.

Quatre disciplines principales ont été retenues : le droit, l’histoire, les sciences religieuses et l’architecture. Elles sont mobilisées de façon variable en fonction de chaque notice.

Plutôt que de viser l’exhaustivité, ce dictionnaire repose sur une présentation des éléments les plus emblématiques du patrimoine religieux, présents non seulement dans le langage courant, mais également dans les langages administratif et académique, en les étayant par la prise en compte du corpus lexical nécessaire à leur compréhension. Le patrimoine religieux s’apparente de ce point de vue à un écosystème : l’entrée peut s’opérer à partir d’un bien, par exemple mobilier, lequel se rattache à un édifice, des usages religieux anciens ou actuels, dont les significations et la compréhension sont accessibles au prisme de l’histoire – dans ses différentes déclinaisons –, des traces architecturales, de leur statut juridique. Il s’agit de dépasser la seule définition pour rendre compte du lien entre le terme faisant l’objet de la notice et le patrimoine religieux.

Le but de la version numérique du Dictionnaire du patrimoine religieux est de favoriser une circulation dans cet espace numérique et de permettre une exploration approfondie du champ lexical du patrimoine religieux par les renvois entre notices. Ce faisant, les données rassemblées et leur interrogation formalisent les liens qui s’établissent entre les biens culturels religieux eux-mêmes et les disciplines pertinentes.

ExPatRe est un projet du Réseau national des Maison des Sciences de l’Homme, porté par la Maison Interuniversitaire des Sciences de l’Homme-Alsace (Strasbourg) en partenariat avec la Maison Européenne des Sciences de l'Homme et de la Société (Lille).

L’équipe du projet
------------------

* Bernard Berthod (Directeur du Musée d’art religieux de Fourvière à Lyon ; consulteur émérite de la Commission pontificale pour les biens culturels de l’Église)
* Marie Cornu (Directrice de recherche CNRS, UMR 7220, *Institut des sciences sociales du politique*)
* Françoise Curtit (Ingénieure de Recherche CNRS, UMR 7354 *Droit, Religion, Entreprise et Société*)
* Anne Fornerod (Chargée de recherche CNRS, UMR 7354 *Droit, Religion, Entreprise et Société*)
* Daniel-Odon Hurel (Directeur de recherche CNRS, UMR 8584 *Laboratoire d’Études sur les monothéismes*)
* Gilles Maury (Architecte, ENSAP Lille, *Laboratoire Conception, Territoire, Histoire*)
* Lucile Pierron (Architecte diplômée d’État, Maître de conférences associée à ENSA Nancy, *Laboratoire d’Histoire de l’Architecture Contemporaine*)

Site réalisé avec le soutien de la `plateforme « humanités numériques » (PHUN) <https://www.misha.fr/plateformes/phun>`_ de la `MISHA <https://www.misha.fr/>`_, et hébergé par l'infrastructure de recherche `Huma-Num <https://www.huma-num.fr/>`_.

Pour la réutilisation d'une illustration, se référer aux indications figurant dans la légende
