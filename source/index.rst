#######################################################
ExPatRe - Dictionnaire du patrimoine culturel religieux
#######################################################


Le projet ExPatRe consiste en la création d’une plateforme numérique destinée à la mise en ligne d'un Dictionnaire du patrimoine culturel religieux.
L'objectif est de parvenir à la détermination d’un champ lexical propre au patrimoine culturel religieux, issu des différents champs disciplinaires mobilisés.

Il est développé dans le cadre d'un projet du RnMSH, co-porté par la MISHA et la MESHS.

.. toctree::
   :hidden:
   :glob:
   :titlesonly:

   Accueil <self>
   Notices </genindex>
   /apropos
