# Configuration file for the Sphinx documentation builder.

# -- Project information -----------------------------------------------------
language = 'fr'

project = 'ExPatRe'
copyright = 'CC BY-NC-SA 3.0'
author = 'Régis Witz'

release = '0.1'
version = '0.1.0'

rst_prolog = """
.. |project| replace:: {project}
.. |version| replace:: {version}
.. |copyright| replace:: {copyright}
""".format(
  project=project,
  version=version,
  copyright=copyright,
)

# -- General configuration ---------------------------------------------------

extensions = [
  'sphinx.ext.autodoc',       # HTML generation from docstrings
  'sphinx.ext.intersphinx',   # Link to other projects’ documentation
  'myst_parser',              # Markdown support
]

intersphinx_mapping = {
  'python': ('https://docs.python.org/3/', None),
  'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output -------------------------------------------------

html_theme = 'pydata_sphinx_theme'

# see: https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html
html_static_path = ['../resource']
html_css_files = ['css/candy.css']
html_logo = '../resource/header.webp'

# -- Options for EPUB output -------------------------------------------------
epub_show_urls = 'footnote'
